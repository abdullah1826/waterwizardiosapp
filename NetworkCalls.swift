//
//  NetworkCalls.swift
//  TacosTheBest
//
//  Created by Muhammad Abdullah on 03/04/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

//class NetworkCalls {
//    static let shared = NetworkCalls()
//    private init(){}
//    
//    typealias ErrorType = (String) -> Void
//    
//    static func homeScreenApi(completion: @escaping (Dictionary<String,Any>) -> Void,
//                           error: @escaping ErrorType) {
//        if Utilites.isInternetAvailable() {
//            let id = User.loggedUser!.user_id!
//            let url = APIConstants.BasePath + APIPaths.cartCount + "/\(id ?? 0)"
//            Alamofire.request(url,
//                              method: .get,
//                              encoding: JSONEncoding.default,
//                              headers: nil).responseJSON { (response) in
//                                switch response.result {
//                                case .success(let value):
//                                    let json = JSON(value)
//                                    print(json)
//                                        let data = json.dictionaryObject!
//                                        completion(data)
//                                case .failure(let err):
//                                    error(err.localizedDescription)
//                                }
//            }
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//
//    static func getMenuItems(completion: @escaping (Dictionary<String,Any>, Json4Swift_Base?) -> Void,
//                           error: @escaping ErrorType) {
//        if Utilites.isInternetAvailable() {
//            let url = APIConstants.BasePath + APIPaths.items
//            Alamofire.request(url,
//                              method: .get,
//                              encoding: JSONEncoding.default,
//                              headers: nil).responseJSON { (response) in
//                                
//                                switch response.result {
//                                case .success(let value):
//                                
//                                    let json = JSON(value)
//                                        print(json)
//                                    
//                                    guard let data = response.data else { return }
//                                    do {
//                                        let decoder = JSONDecoder()
//                                        let responseData = try! decoder.decode(Json4Swift_Base.self, from: data)
//                                        print(responseData)
//                                        let dataObject = json.dictionaryObject!
//                                            completion(dataObject, responseData)
//                                    } catch _ {
//                                        
//                                    }
//                                    
//                                case .failure(let err):
//                                    error(err.localizedDescription)
//                                }
//                            }
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    
//    static func getSubMenuItems(completion: @escaping (Dictionary<String,Any>) -> Void,
//                           error: @escaping ErrorType) {
//        if Utilites.isInternetAvailable() {
//            let url = APIConstants.BasePath + APIPaths.submenu
//            Alamofire.request(url,
//                              method: .get,
//                              encoding: JSONEncoding.default,
//                              headers: nil).responseJSON { (response) in
//                                switch response.result {
//                                case .success(let value):
//                                    let json = JSON(value)
//                                    print(json)
//                                        let data = json.dictionaryObject!
//                                        completion(data)
//                                case .failure(let err):
//                                    error(err.localizedDescription)
//                                }
//                            }
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    
//    static func addToCart(_ params: [String:Any],completion: @escaping (Dictionary<String,Any>) -> Void,
//                          error: @escaping ErrorType){
//        
//        if Utilites.isInternetAvailable() {
//            
//            let jsonData = try? JSONSerialization.data(withJSONObject: params)
//            
//            let url = APIConstants.BasePath + APIPaths.addcart
//            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
//                                              cachePolicy: .useProtocolCachePolicy,
//                                              timeoutInterval: 10.0)
//            request.httpMethod = "POST"
//            request.allHTTPHeaderFields = nil
//            request.httpBody = jsonData
//            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//            request.setValue("\(jsonData!.count)", forHTTPHeaderField: "Content-Length")
//
//            let session = URLSession.shared
//            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//                if (error != nil) {
//                    print(error!)
//                } else {
//                    
//                    do {
//                        
//                        if let response = response {
//                            print("url = \(response.url!)")
//                            print("response = \(response)")
//                            let httpResponse = response as! HTTPURLResponse
//                            print("response code = \(httpResponse.statusCode)")
//                        }
//                        
//                        guard let data = data, let _:URLResponse = response, error == nil else {
//                            print("error")
//                            return
//                        }
//
//                        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//                        if let responseJSON = responseJSON as? [String: Any] {
//                            print(responseJSON)
//                            completion(responseJSON)
//                        }
//                        
//                    } catch let err {
//                        print("\nError", err)
//                    }
//                }
//            })
//            
//            dataTask.resume()
//            
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    
//    static func updateCartItem(_ url: String, _ instruction: String,completion: @escaping (Dictionary<String,Any>) -> Void,
//                          error: @escaping ErrorType){
//        
//        if Utilites.isInternetAvailable() {
//            print(url)
////            let components = NSURLComponents(string: url)!
////            let queryItem = NSURLQueryItem(name: "item_instruction", value: instruction)
////            // add the query item to the URL, NSURLComponents takes care of adding the question mark.
////            components.queryItems = [queryItem]
////            // get the properly percent encoded string
////            url = components.string
//            let request = NSMutableURLRequest(url: NSURL(string: url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)! as URL,
//                                              cachePolicy: .useProtocolCachePolicy,
//                                              timeoutInterval: 10.0)
//            
//
//            request.httpMethod = "PUT"
//            request.allHTTPHeaderFields = nil
//            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//            
//            let session = URLSession.shared
//            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//                if (error != nil) {
//                    print(error!)
//                } else {
//                    
//                    do {
//                        
//                        if let response = response {
//                            print("url = \(response.url!)")
//                            print("response = \(response)")
//                            let httpResponse = response as! HTTPURLResponse
//                            print("response code = \(httpResponse.statusCode)")
//                        }
//                        
//                        guard let data = data, let _:URLResponse = response, error == nil else {
//                            print("error")
//                            return
//                        }
//
//                        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//                        if let responseJSON = responseJSON as? [String: Any] {
//                            print(responseJSON)
//                            completion(responseJSON)
//                        }
//                        
//                    } catch let err {
//                        print("\nError", err)
//                    }
//                }
//            })
//            
//            dataTask.resume()
//            
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    static func getSignleCartItem(_ params:[String:Any] ,completion: @escaping (CartSingleItem) -> Void,
//                           error: @escaping ErrorType) {
//        if Utilites.isInternetAvailable() {
//            let url = APIConstants.BasePath + APIPaths.getSignleCartItem
//            Alamofire.request(url,
//                              method: .post,
//                              parameters:params,
//                              encoding: JSONEncoding.default,
//                              headers: nil).responseJSON { (response) in
//                                switch response.result {
//                                case .success(let value):
//                                    let json = JSON(value)
//                                    print(json)
//
//                                    guard let data = response.data else { return }
//                                    
//                                    do {
//                                        let decoder = JSONDecoder()
//                                        let responseData = try! decoder.decode(CartSingleItem.self, from: data)
//                                        print(responseData)
//                                        completion(responseData)
//                                    } catch _ {
//                                }
//                                    
//                                case .failure(let err):
//                                    error(err.localizedDescription)
//                                }
//                            }
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    static func getCartItems(_ params:[String:Any] ,completion: @escaping (CartItem) -> Void,
//                           error: @escaping ErrorType) {
//        if Utilites.isInternetAvailable() {
//            let url = APIConstants.BasePath + APIPaths.getcart
//            Alamofire.request(url,
//                              method: .post,
//                              parameters:params,
//                              encoding: JSONEncoding.default,
//                              headers: nil).responseJSON { (response) in
//                                switch response.result {
//                                case .success(let value):
//                                    let json = JSON(value)
//                                    print(json)
//
//                                    guard let data = response.data else { return }
//                                    
//                                    do {
//                                        let decoder = JSONDecoder()
//                                        let responseData = try! decoder.decode(CartItem.self, from: data)
//                                        print(responseData)
//                                        completion(responseData)
//                                    } catch _ {
//                                    }
//                                    
//                                case .failure(let err):
//                                    error(err.localizedDescription)
//                                }
//                            }
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//
//    static func deleteCartItems(_ params: [String:Any],completion: @escaping (Dictionary<String,Any>) -> Void,
//                          error: @escaping ErrorType){
//        
//        if Utilites.isInternetAvailable() {
//            
//          //  let jsonData = try? JSONSerialization.data(withJSONObject: params)
//            
//            let url = APIConstants.BasePath + APIPaths.deletecart + "/\(params["item_id"]!)" + "?user_id=\(params["user_id"]!)"
//            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
//                                              cachePolicy: .useProtocolCachePolicy,
//                                              timeoutInterval: 10.0)
//            request.httpMethod = "DELETE"
//            request.allHTTPHeaderFields = nil
//            request.httpBody = nil
//            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//          //  request.setValue("\(jsonData!.count)", forHTTPHeaderField: "Content-Length")
//
//            let session = URLSession.shared
//            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//                if (error != nil) {
//                    print(error!)
//                } else {
//                    
//                    do {
//                        
//                        if let response = response {
//                            print("url = \(response.url!)")
//                            print("response = \(response)")
//                            let httpResponse = response as! HTTPURLResponse
//                            print("response code = \(httpResponse.statusCode)")
//                        }
//                        
//                        guard let data = data, let _:URLResponse = response, error == nil else {
//                            print("error")
//                            return
//                        }
//
//                        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//                        if let responseJSON = responseJSON as? [String: Any] {
//                            print(responseJSON)
//                            completion(responseJSON)
//                        }
//                        
//                    } catch let err {
//                        print("\nError", err)
//                    }
//
//
//                }
//            })
//            
//            dataTask.resume()
//            
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    
//    static func cancelOrder(_ order_id: Int,completion: @escaping (Dictionary<String,String>) -> Void,
//                          error: @escaping ErrorType){
//        
//        if Utilites.isInternetAvailable() {
//                        
//            let url = APIConstants.BasePath + APIPaths.cancelOrder + "/order_id=\(order_id)"
//            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
//                                              cachePolicy: .useProtocolCachePolicy,
//                                              timeoutInterval: 10.0)
//            request.httpMethod = "DELETE"
//            request.allHTTPHeaderFields = nil
//            request.httpBody = nil
//            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//
//            let session = URLSession.shared
//            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//                if (error != nil) {
//                    print(error!)
//                } else {
//                    
//                    do {
//
//                        guard let data = data, let _:URLResponse = response, error == nil else {
//                            print("error")
//                            return
//                        }
//                        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//                        if let responseJSON = responseJSON as? [String: String] {
//                            print(responseJSON)
//                            completion(responseJSON)
//                        }
//                        
//                    } catch let err {
//                        print("\nError", err)
//                    }
//
//
//                }
//            })
//            
//            dataTask.resume()
//            
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    
//    static func getLanguage(completion: @escaping (Translation) -> Void,
//                               error: @escaping ErrorType) {
//            if Utilites.isInternetAvailable() {
//                let url = APIConstants.BasePath + APIPaths.translator
//                Alamofire.request(url,
//                                  method: .get,
//                                  parameters:nil,
//                                  encoding: JSONEncoding.default,
//                                  headers: nil).responseJSON { (response) in
//                                    switch response.result {
//                                    case .success(let value):
//                                        let json = JSON(value)
//                                        print(json)
//
//                                        guard let data = response.data else { return }
//                                        
//                                        do {
//                                            let decoder = JSONDecoder()
//                                            let responseData = try? decoder.decode(Translation.self, from: data)
//                                            print(responseData!)
//                                            Utilites.appLanguage = responseData
//                                            completion(responseData!)
//                                        } catch _ {
//                                    }
//                                        
//                                    case .failure(let err):
//                                        error(err.localizedDescription)
//                                    }
//                                }
//            } else {
//                error(AlertConstants.InternetNotReachable)
//            }
//        }
//}
