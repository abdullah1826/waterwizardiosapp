//
//  ExtPh.swift
//  WaterWizard
//
//  Created by Muhammad Abdullah on 06/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import Foundation

extension CalculaionsViewController {
    
    
    func slider1greater()-> Bool{
        var value1 = convertFloatTo1Decimal(firstSlider.value)
        var value2 = convertFloatTo1Decimal(secondSlider.value)
        if value1 > value2 {
            return true
        } else
        if (value1 < value2) {
            return false
        }
        return false
    }
    
    func isSameValue(_ value1: Float, _ value2: Float)  -> Bool? {
        let range = Float(String(format: "%.1f", value2))!
        if value1 == range {
            displyMessagelbl.text = "Current PH and Desired PH are on the same level"
            //            displyMessagelbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            clearImage()
            instruction1lbl.isHidden = true
            instruction2lbl.isHidden = true
            displayOR.isHidden = true
            return true
        } else {return false}
    }
    func isSameValue1(_ value1: Float, _ value2: Float)  -> Bool? {
        let firstvalue = convertFloatTo1Decimal(value1)
        let secondvalue = convertFloatTo1Decimal(value2)
        if (firstvalue == secondvalue) {
            return true
        }else {
            return false
        }
    }
    
    
    func calculateChemicalAmount(_ value: Float, _ volume: Float, _ difference: Float) -> Float {
        if pharray.contains((value)) {
            print(value)
//            pharray.index
//            let indexOfValue = pharray.indexes(of: String(value)) // 0
            let currentIndex = pharray.firstIndex(of: value)
//             let chemicalAmount2 = Float(currentIndex! + 1)
            let chemicalAmount = Float(currentIndex! + 1) * difference * volume
          
            return chemicalAmount
        } else {
            print("nil")
            return 0.0
        }
    }
    func calculateChemicalAmountAlkalinity(_ value: Float, _ volume: Float, _ difference: Float) -> Float {
        if floatArray.contains(Int(value)) {
            print(value)
            //            pharray.index
            //            let indexOfValue = pharray.indexes(of: String(value)) // 0
            let currentIndex = floatArray.firstIndex(of: Int(value))
            //             let chemicalAmount2 = Float(currentIndex! + 1)
            let chemicalAmount = Float(currentIndex! + 1) * difference * volume
            
            return chemicalAmount
        } else {
            print("nil")
            return 0.0
        }
    }
    func calculateChemicalAmountAlkalinityApp(_ value: Float, _ volume: Float, _ difference: Float) -> Float {
        if floatArray.contains(Int(value)) {
            var initialvalue = volume * difference * 10
            print(value)
            //            pharray.index
            //            let indexOfValue = pharray.indexes(of: String(value)) // 0
            let currentIndex = floatArray.firstIndex(of: Int(value))
            //             let chemicalAmount2 = Float(currentIndex! + 1)
            let chemicalAmount = Float(currentIndex!) * difference * volume
            var amounttosend = initialvalue + chemicalAmount
            if currentIndex == 0 {
                return initialvalue
            }else{
            return amounttosend
            }
        } else {
            print("nil")
            return 0.0
        }
    }
        
    
    
    func displayImage() -> (String,String) {
        
        let slider1 = Double(String(format: "%.1f", self.firstSlider.value))
        let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
        
        // when desire is set higher than current
        if slider1!.isLess(than: slider2!) {
            print("PH UP")
            displayOR.isHidden = true
            instruction2lbl.isHidden = true
            //            instruction1lbl.isHidden = false
            self.firstImage.image = UIImage(named:Chemical.ph_up)
            return ("PH UP","")
            
        } else if slider2!.isLess(than: slider1!) {
            // when desire is set lower than current
            displayOR.isHidden = false
            instruction1lbl.isHidden = false
            instruction2lbl.isHidden = false
            print("Muriatic Acid or PH Down")
            self.secondImage.image = UIImage(named:Chemical.muriatic_acid)
            self.firstImage.image = UIImage(named: Chemical.ph_down)
            return ("PH DOWN","Muriatic Acid")
        }
        
        return ("","")
    }
    
    func clearImage(){
        firstImage.image = UIImage(named: "")
        secondImage.image = UIImage(named: "")
    }
    
    func hideAllIBOutlets(){
        instruction1lbl.isHidden = true
        instruction2lbl.isHidden = true
        displayOR.isHidden = true
    }
    
    func showAllIBOutlets(){
        instruction1lbl.isHidden = false
        instruction2lbl.isHidden = false
        displayOR.isHidden = false
    }
    
    
    func convertFloatTo1Decimal(_ value: Float)-> Float {
        let range = Float(String(format: "%.1f", value))!
        print(range)
        return range
    }
    func displayMessagePH(_ value: Float){
        
        print(value)
        let slider1 = firstSlider.value
        let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
        switch (slider1) {
        case 7.4...7.6:
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current PH and the desired PH are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
                break
            }
            
            clearImage()
            hideAllIBOutlets()
            displyMessagelbl.text = "PH is with in recomended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
            
            
            break
            
            
        case (7.2...7.3),(7.7...7.8):
            
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Chlorine and the desired Chlorine are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
                clearImage()
                hideAllIBOutlets()
                break
            }
            displyMessagelbl.isHidden = false
            displyMessagelbl.text = "PH is within the health regulation range to bring this into recommended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            var amount: Float!
            let message = slider1greater()
                       print(message)
                       if message == true {
                           instruction1lbl.isHidden = false
                           firstImage.isHidden = false
                           instruction2lbl.isHidden = false
                           secondImage.isHidden = false
                           // found muriatic acid
                           amount = calculateChemicalAmount(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(muriaticAcid))
                        var amount1 = String(format: "%.2f", amount)
                        instruction1lbl.text = "Please add \(amount1) \(Chemical.unitMl) of Muriatic Acid"
                           instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                           ("this is our amount\(amount)")

                           self.firstImage.image = UIImage(named:Chemical.muriatic_acid)
                           self.secondImage.image = UIImage(named: Chemical.ph_down)
                           
                           amount = calculateChemicalAmount(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(ph))
                        var amount2 = String(format: "%.2f", amount)
                           instruction2lbl.text = "Please add \(amount2) \(Chemical.unitMl) of PH Down"
                           instruction2lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                          
                           
                           
                       } else {
                           instruction1lbl.isHidden = false
                           firstImage.isHidden = false
                           amount = calculateChemicalAmount(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(ph))
                        var amount1 = String(format: "%.2f", amount)
                                          instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of PH UP "
                                          instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                            self.firstImage.image = UIImage(named:Chemical.ph_up)
                       }
            
            break
            
       case (6.2...7.2), (7.8...8.4):
            
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Chlorine and the desired Chlorine are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                
                clearImage()
                hideAllIBOutlets()
                break
            }
            
            var amount: Float!
            
            
            displyMessagelbl.isHidden = false
            displyMessagelbl.text = "Chlorine is out of balance, to bring this into recommended range "
            displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            let message = slider1greater()
            print(message)
            if message == true {
                instruction1lbl.isHidden = false
                firstImage.isHidden = false
                instruction2lbl.isHidden = false
                secondImage.isHidden = false
                // found muriatic acid
                amount = calculateChemicalAmount(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(muriaticAcid))
                var amount1 = String(format: "%.2f", amount)
                instruction1lbl.text = "Please add \(amount1) \(Chemical.unitMl) of Muriatic Acid"
                instruction1lbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                ("this is our amount\(amount)")

                self.firstImage.image = UIImage(named:Chemical.muriatic_acid)
                self.secondImage.image = UIImage(named: Chemical.ph_down)
                
                amount = calculateChemicalAmount(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(ph))
                var amount2 = String(format: "%.2f", amount)
                instruction2lbl.text = "Please add \(amount2) \(Chemical.unitMl) of PH Down"
                instruction2lbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                               
                
                
            } else {
                instruction1lbl.isHidden = false
                firstImage.isHidden = false
                amount = calculateChemicalAmount(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(ph))
                var amount1 = String(format: "%.2f", amount)
                               instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of PH UP "
                               instruction1lbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                 self.firstImage.image = UIImage(named:Chemical.ph_up)
            }
            
            
            break
            
        default:
            break
        }
    }
    
    
    
    // methods for the chlorine
    
    func displayMessageC(_ value: Float){
        
        print(value)
        let slider1 = firstSlider.value
        let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
        switch (slider1) {
        case 2.0...4.0:
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Chlorine and the desired Chlorine are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
                break
            }
            
            clearImage()
            hideAllIBOutlets()
            displyMessagelbl.text = "Chlorine is with in recomended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
            
            
            break
            
            
        case 4.1...10.0:
            
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Chlorine and the desired Chlorine are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
                clearImage()
                hideAllIBOutlets()
                break
            }
            displyMessagelbl.isHidden = false
            displyMessagelbl.text = "Chlorine is within the health regulation range to bring this into recommended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            var amount: Float!
            let message = displayImageC()
            if message.1 != "" {
                // found muriatic acid
                let checkvalue = secondSlider.value
                print(checkvalue)
                
                
                amount = calculatechlorine()
                let stringFloat =  String(describing: amount!)
                var amount1 = String(format: "%.2f", amount)
                instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of Chlore-out"
                instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
                
                
                
            } else {
                
                let k: Float = 0.002
                let j: Float = 0.000769
                amount = calculatechlorine2(i: k)
                let stringFloat =  String(describing: amount!)
                var amount1 = String(format: "%.2f", amount)
                instruction1lbl.text = "Please add \(amount1) \(Chemical.unitPpm) of Liquid Chlorine(Sodium Hypochlorite 10.8%)"
                instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                amount = calculatechlorine2(i: j)
                let stringFloat1 =  String(describing: amount!)
                
                instruction2lbl.text = "You can add \(stringFloat1)\(Chemical.unitGram) of Granular Shock"
                instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
            break
            
        case 0.0...2.0:
            
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Chlorine and the desired Chlorine are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                
                clearImage()
                hideAllIBOutlets()
                break
            }
            
            var amount: Float!
            
            
            displyMessagelbl.isHidden = false
            displyMessagelbl.text = "Chlorine is out of balance, to bring this into recommended range "
            displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            let message = displayImageC()
            print(message)
            if message.1 != "" {
                // found muriatic acid
                
                amount = calculatechlorine()
                let stringFloat =  String(describing: amount!)
                var amount1 = String(format: "%.2f", amount)
                
                instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of Chlor-out"
                
                
                
            } else {
                
                let k: Float = 0.002
                let j: Float = 0.000769
                amount = calculatechlorine2(i: k)
                let stringFloat =  String(describing: amount!)
                //                           displyMessagelbl.text = "Chlorine is out of balance, to bring this into recommended range "
                //                 displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                var amount1 = String(format: "%.2f", amount)
                instruction1lbl.text = "Please add \(amount1) \(Chemical.unitPpm) of Liquid Chlorine(Sodium Hypochlorite 10.8%)"
                instruction1lbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                amount = calculatechlorine2(i: j)
                let stringFloat1 =  String(describing: amount!)
                var amount2 = String(format: "%.2f", amount)
                instruction2lbl.text = "You can add \(amount2) \(Chemical.unitGram) of Granular Shock"
                instruction2lbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            }
            
            
            break
            
        default:
            break
        }
    }
    
    func displayImageC() -> (String,String) {
        
        let slider1 = Double(String(format: "%.1f", self.firstSlider.value))
        let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
        
        // when desire is set higher than current
        if slider1!.isLess(than: slider2!) {
            print("PH UP")
            displayOR.isHidden = false
            instruction2lbl.isHidden = false
            instruction1lbl.isHidden = false
            self.firstImage.image = UIImage(named:Chemical.liquid_chlorine)
            self.secondImage.image = UIImage(named:Chemical.shock_chlorine)
            return ("PH UP","")
            
        } else if slider2!.isLess(than: slider1!) {
            // when desire is set lower than current
            displayOR.isHidden = true
            instruction1lbl.isHidden = false
            instruction2lbl.isHidden = true
            print("Muriatic Acid or PH Down")
            //                self.secondImage.image = UIImage(named:Chemical.muriatic_acid)
            self.firstImage.image = UIImage(named: Chemical.chlor_out)
            self.secondImage.image = UIImage(named: "")
            return ("PH Down","Muriatic Acid")
        }
        
        return ("","")
    }
    func displayImageStabilizer() -> (String,String) {
        
        let slider1 = Double(String(format: "%.1f", self.firstSlider.value))
        let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
        
        // when desire is set higher than current
        if slider1!.isLess(than: slider2!) {
            print("PH UP")
            displayOR.isHidden = true
            instruction2lbl.isHidden = true
            instruction1lbl.isHidden = false
            self.firstImage.image = UIImage(named:Chemical.stabilizer)
            self.secondImage.isHidden = true
            return ("PH UP","")
            
        } else if slider2!.isLess(than: slider1!) {
            // when desire is set lower than current
            displayOR.isHidden = true
            instruction2lbl.isHidden = true
            instruction1lbl.isHidden = false
            self.firstImage.image = UIImage(named:Chemical.stabilizer)
            self.secondImage.isHidden = true
            return ("","Muriatic Acid")
        }
        
        return ("","")
    }
    
    func calculatechlorine()-> Float {
        var secondslidervalue = convertFloatTo1Decimal(secondSlider.value)
        var amount = secondslidervalue * Float(volumetoshow)
        return amount
        
    }
    func calculatechlorine2(i: Float)-> Float{
        var secondslidervalue = convertFloatTo1Decimal(secondSlider.value)
        var amount = (secondslidervalue * Float(volumetoshow) * 1000 * i) / 2
        print("this is sweet amount\(amount)")
        return amount
        
    }
    func displayMessageA(_ value: Float){
        
        print(value)
        let slider1 = firstSlider.value
        let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
        switch (slider1) {
        case 81...120:
            
            
            clearImage()
            hideAllIBOutlets()
            displyMessagelbl.text = "Alkalanity is within recommended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
            
            
            break
            
            
        case 0...80:
            
            
            displyMessagelbl.isHidden = false
            firstImage.isHidden = false
            displyMessagelbl.text = "Alkalanity is out of balance, to bring this into recommended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            var amount : Float!
            amount = calculateChemicalAmountAlkalinityApp(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(alkalinity))
            let stringFloat =  String(describing: amount!)
            var amount1 = String(format: "%.2f", amount)
            instruction1lbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of buffer"
            self.firstImage.image = UIImage(named: Chemical.buffer)
            
            break
            
        case 121...200:
            
            displyMessagelbl.isHidden = false
            displyMessagelbl.text = "Alkalanity is out of balance, to bring this into recommended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            var amount : Float!
           amount = calculateChemicalAmountAlkalinityApp(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(alkalinity))
            let stringFloat =  String(describing: amount!)
            var amount1 = String(format: "%.2f", amount)
            instruction1lbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            instruction1lbl.isHidden = false
            instruction1lbl.text = "Please use PH calculator"
            self.firstImage.isHidden = true
            instruction2lbl.isHidden = true
            self.secondImage.isHidden = true
            
            
            break
            
        default:
            break
        }
    }
    func displayMessageB(_ value: Float){
        let firstSlidervalue = convertFloatTo1Decimal(firstSlider.value)
        let secondSlidervalue = convertFloatTo1Decimal(secondSlider.value)
        print(value)
        let slider1 = firstSlider.value
        let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
        switch (slider1) {
        case 0.0...10.0:
            if firstSlidervalue >= secondSlidervalue {
                displyMessagelbl.isHidden = false
                displyMessagelbl.text = "Total chlorine cannot be lower than free chlorine"
                displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                
                hideAllIBOutlets()
                firstImage.isHidden = true
                secondImage.isHidden = true
            }else{
                displyMessagelbl.isHidden = false
                displyMessagelbl.text = "Your combine chlorine level looks great"
                displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
                
                hideAllIBOutlets()
                firstImage.isHidden = true
                secondImage.isHidden = true
            }
            break
            
        default:
            break
        }
    }
    
    func displayMessageS(_ value: Float){
        
        print(value)
        let slider1 = firstSlider.value
        let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
        let firstSlidervalue = convertFloatTo1Decimal(firstSlider.value)
        let secondSlidervalue = convertFloatTo1Decimal(secondSlider.value)
        switch (slider1) {
        case 30.1...40.0:
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Stabilizer and the desired Stabilizer are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
                break
            }
            
            clearImage()
            hideAllIBOutlets()
            displyMessagelbl.text = "Stabilier is within recomended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
            
            
            break
            
            
        case (10.1...30.0), (40.1...60.0):
            
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Stabilizer and the desired Stabilizer are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
                clearImage()
                hideAllIBOutlets()
                break
            }
            displyMessagelbl.isHidden = false
            displyMessagelbl.text = "Chlorine is within the health regulation range to bring this into recommended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            if firstSlidervalue < secondSlidervalue {
                var amount: Float!
                amount = calculateChemicalAmountAlkalinity(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(stabilizer))
                let stringFloat =  String(describing: amount!)
                var amount1 = String(format: "%.2f", amount)
                instruction1lbl.isHidden = false
                firstImage.isHidden = false
                instruction2lbl.isHidden = true
                secondImage.isHidden = true
                displayOR.isHidden = true
                self.firstImage.image = UIImage(named:Chemical.chlor_out)
                instruction1lbl.text = "Please add \(amount1) \(Chemical.unitMl) of Cyanuric Acid"
                instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
            }else{
                
                instruction1lbl.isHidden = false
                firstImage.isHidden = false
                instruction2lbl.isHidden = true
                secondImage.isHidden = true
                displayOR.isHidden = true
                self.firstImage.image = UIImage(named:Chemical.chlor_out)
                instruction1lbl.text = "Please add fresh water"
                instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
            }
            
            break
            
        case 60.0...100.0:
            
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Stabilizer and the desired Stabilizer are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                
                clearImage()
                hideAllIBOutlets()
                break
            }
            displyMessagelbl.isHidden = false
            displyMessagelbl.text = "Stabilier is out of balance to bring this into recommended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            instruction1lbl.isHidden = false
            firstImage.isHidden = false
            instruction2lbl.isHidden = true
            secondImage.isHidden = true
            displayOR.isHidden = true
            self.firstImage.image = UIImage(named:Chemical.chlor_out)
            instruction1lbl.text = "Please add fresh water"
            instruction1lbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            break
            
        case 0.0...10.0:
            
            let same = isSameValue1(firstSlider.value, secondSlider.value)
            
            if same == true{
                displyMessagelbl.text = "Current Stabilizer and the desired Stabilizer are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                
                clearImage()
                hideAllIBOutlets()
                break
            }
            displyMessagelbl.isHidden = false
            displyMessagelbl.text = "Stabilier is out of balance to bring this into recommended range"
            displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            var amount: Float!
            amount = calculateChemicalAmountAlkalinityApp(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(stabilizer))
            let stringFloat =  String(describing: amount!)
            var amount1 = String(format: "%.2f", amount)
            instruction1lbl.isHidden = false
            firstImage.isHidden = false
            instruction2lbl.isHidden = true
            secondImage.isHidden = true
            displayOR.isHidden = true
            self.firstImage.image = UIImage(named:Chemical.chlor_out)
            instruction1lbl.text = "Please add \(amount1) \(Chemical.unitMl) of Cyanuric Acid"
            instruction1lbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
            break
            
        default:
            break
        }
    }
    func displayMessageBromine(_ value: Float){
          
          print(value)
          let slider1 = firstSlider.value
          let slider2 = Double(String(format: "%.1f", self.secondSlider.value))
          let firstSlidervalue = convertFloatTo1Decimal(firstSlider.value)
          let secondSlidervalue = convertFloatTo1Decimal(secondSlider.value)
          switch (slider1) {
          case 7.1...8.0:
              let same = isSameValue1(firstSlider.value, secondSlider.value)
              
              if same == true{
                  displyMessagelbl.text = "Current Stabilizer and the desired Stabilizer are at the same level"
                  displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
                  break
              }
              
              clearImage()
              hideAllIBOutlets()
              displyMessagelbl.text = "Bromine is within recomended range"
              displyMessagelbl.textColor = #colorLiteral(red: 0.2, green: 0.768627451, blue: 0.09019607843, alpha: 1)
              
              
              break
              
              
          case (5.1...7.0), (8.1...10.0):
              
              let same = isSameValue1(firstSlider.value, secondSlider.value)
              
              if same == true{
                  displyMessagelbl.text = "Current Stabilizer and the desired Stabilizer are at the same level"
                  displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                  
                  clearImage()
                  hideAllIBOutlets()
                  break
              }
              displyMessagelbl.isHidden = false
              displyMessagelbl.text = "Bromine is within the health regulation range to bring this into recommended range"
              displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
              
              if firstSlidervalue < secondSlidervalue {
                  var amount: Float!
                  amount = calculateChemicalAmountAlkalinity(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(neutralizer))
                  let stringFloat =  String(describing: amount!)
                var amount1 = String(format: "%.2f", amount)
                  instruction1lbl.isHidden = false
                  firstImage.isHidden = false
                  instruction2lbl.isHidden = true
                  secondImage.isHidden = true
                  displayOR.isHidden = true
                  self.firstImage.image = UIImage(named:Chemical.neutralizer)
                  instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of Neutralizer"
                  instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                  
              }else{
                  var amount: Float!
                                   amount = calculateChemicalAmountAlkalinity(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(bromine))
                let stringFloat =  String(describing: amount!)
                var amount1 = String(format: "%.2f", amount)
                  instruction1lbl.isHidden = false
                  firstImage.isHidden = false
                  instruction2lbl.isHidden = true
                  secondImage.isHidden = true
                  displayOR.isHidden = true
                  self.firstImage.image = UIImage(named:Chemical.brome)
                 self.firstImage.image = UIImage(named:Chemical.neutralizer)
                                   instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of Neutralizer"
                                   instruction1lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                  
              }
              
              break
              
          case (0.0...4.0), (10.1...20.0):
               let same = isSameValue1(firstSlider.value, secondSlider.value)
               
               if same == true{
                displyMessagelbl.text = "Current Stabilizer and the desired Stabilizer are at the same level"
                displyMessagelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
                clearImage()
                hideAllIBOutlets()
                break
               }
               displyMessagelbl.isHidden = false
               displyMessagelbl.text = "Bromine is within the health regulation range to bring this into recommended range"
               displyMessagelbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
               
               if firstSlidervalue < secondSlidervalue {
                var amount: Float!
                amount = calculateChemicalAmountAlkalinity(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(neutralizer))
                let stringFloat =  String(describing: amount!)
                var amount1 = String(format: "%.2f", amount)
                instruction1lbl.isHidden = false
                firstImage.isHidden = false
                instruction2lbl.isHidden = true
                secondImage.isHidden = true
                displayOR.isHidden = true
                self.firstImage.image = UIImage(named:Chemical.neutralizer)
                instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of Neutralizer"
                instruction1lbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                
               }else{
                var amount: Float!
                amount = calculateChemicalAmountAlkalinity(convertFloatTo1Decimal(secondSlider.value), Float(volumetoshow) * 1000, Float(bromine))
                var amount1 = String(format: "%.2f", amount)
                let stringFloat =  String(describing: amount!)
                instruction1lbl.isHidden = false
                firstImage.isHidden = false
                instruction2lbl.isHidden = true
                secondImage.isHidden = true
                displayOR.isHidden = true
                self.firstImage.image = UIImage(named:Chemical.brome)
                self.firstImage.image = UIImage(named:Chemical.neutralizer)
                instruction1lbl.text = "Please add \(amount1) \(Chemical.unitGram) of Neutralizer"
                instruction1lbl.textColor = #colorLiteral(red: 0.9450980392, green: 0.1176470588, blue: 0, alpha: 1)
                
                          }
              break
              
         default:
              break
          }
      }
      
    
    
    
    func calculateChemicalAmountAlkalanity(_ value: Float, _ volume: Float, _ difference: Float) -> Float {
        if floatArray.contains(Int(value)) {
            print(value)
            var value1 = convertFloatTo1Decimal(value)
            let currentIndex = floatArray.firstIndex(of: Int(value1))
             let chemicalAmount2 = Float(currentIndex! + 1)
            let chemicalAmount = Float(currentIndex! + 1) * difference * volume
            print(currentIndex)
            print(chemicalAmount2)
            print(chemicalAmount)
            return chemicalAmount
        } else {
            print("nil")
            return 0.0
        }
    }
    
    
}
