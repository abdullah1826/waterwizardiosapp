//
//  HomeViewController.swift
//  Created by Muhammad Abdullah on 04/06/2020.
//

import UIKit
import WMSegmentControl

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var segmentController: WMSegment!
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    var level = UserDefaults.standard.integer(forKey: "levelsaved")
    
    var spaArray = ["PH","CHLORINE","ALKALINITY","BROMINE","STABILIZER","ENZYMES"]
    var poolArray = ["PH","CHLORINE","ALKALINITY","BREAKPOINT","STABILIZER","ALGECIDE","ENZYMES","PHOSPHATE"]
    var waterfeatureArray = ["PH","ALKALINITY","BROMINE","CHLORINE","STABILIZER"]
    
    var selectedSegmentIndex: Int = 0    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        navigationbarLogo()
        setSegmentController()
        
        let stafNib = UINib(nibName: "OptionsCollectionViewCell", bundle: nil)
        optionsCollectionView.register(stafNib, forCellWithReuseIdentifier: "cell")
        
        optionsCollectionView.delegate = self
        optionsCollectionView.dataSource = self
        
        setCollectionLayout()
        
    }
    
    func setCollectionLayout() {
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:20,left:0,bottom:0,right:0)
        layout.itemSize = CGSize(width:optionsCollectionView.frame.width - 50 , height: 70)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        optionsCollectionView.collectionViewLayout = layout
    }
    
    func navigationbarLogo(){
        
        let image = UIImage(named:"banner.png")
        let imageView = UIImageView(image: image)
        
        imageView.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        self.navigationItem.titleView = imageView
        
    }
    
    
    
    func setSegmentController() {
        segmentController.type = .normal
        segmentController.selectorType = .bottomBar
        segmentController.selectorColor = Utilites.appColor
        segmentController.normalFont = UIFont(name: "ChalkboardSE-Regular", size: 15)!
        segmentController.SelectedFont = UIFont(name: "ChalkboardSE-Bold", size: 15)!
    }
    
    @IBAction func tappedSegmentController(_ sender: WMSegment) {
        
        selectedSegmentIndex = sender.selectedSegmentIndex
        self.optionsCollectionView.reloadData()
        
        switch sender.selectedSegmentIndex {
        case 0:
            break
        case 1:
            break
        case 2:
            break
        default:
            break
            
        }
        
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if selectedSegmentIndex == 0 {
            return self.spaArray.count
        }
        if selectedSegmentIndex == 1 {
            return self.poolArray.count
        }
        if selectedSegmentIndex == 2 {
            return self.waterfeatureArray.count
        }
        
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controllerRef = self.getControllerRef(controller: CalculaionsViewController.id, storyboard: Storyboards.Main.id) as! CalculaionsViewController
        if selectedSegmentIndex == 0 {
            
            switch level {
            case 1:
                let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            case 2:
                let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            case 3:
                controllerRef.didSelect = spaArray[indexPath.row]
                controllerRef.volumetoshow =  UserDefaults.standard.integer(forKey: "spavolume")
                 self.navigationController?.pushViewController(controllerRef, animated: true)
                break
            case 4:
                break
            case 5:
                if indexPath.row <= 2{
                    controllerRef.didSelect = spaArray[indexPath.row]
                    controllerRef.volumetoshow =  UserDefaults.standard.integer(forKey: "spavolume")
                    self.navigationController?.pushViewController(controllerRef, animated: true)
                }else{
                    let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                break
                        case 6:
                            controllerRef.didSelect = spaArray[indexPath.row]
                            controllerRef.volumetoshow =  UserDefaults.standard.integer(forKey: "spavolume")
                             self.navigationController?.pushViewController(controllerRef, animated: true)
                    break
                  default:
                      break
                      
                  }
            
            

       }
        if selectedSegmentIndex == 1 {
            switch level {
                  case 1:
                    if indexPath.row < 2 {
                           controllerRef.didSelect = poolArray[indexPath.row]
                           controllerRef.volumetoshow = UserDefaults.standard.integer(forKey: "poolvolume")
                         self.navigationController?.pushViewController(controllerRef, animated: true)
                                   }else{
                       let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                                       }
                      break
                  case 2:
                      controllerRef.didSelect = poolArray[indexPath.row]
                                      controllerRef.volumetoshow = UserDefaults.standard.integer(forKey: "poolvolume")
                                       self.navigationController?.pushViewController(controllerRef, animated: true)
                      break
                  case 3:
                    let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                   self.present(alert, animated: true, completion: nil)
                      break
                case 4:
                    let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    break
                case 5:
                    if indexPath.row <= 2{
                        controllerRef.didSelect = poolArray[indexPath.row]
                        controllerRef.volumetoshow = UserDefaults.standard.integer(forKey: "poolvolume")
                         self.navigationController?.pushViewController(controllerRef, animated: true)
                        
                    }else{
                        let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    break
                case 6:
                    controllerRef.didSelect = poolArray[indexPath.row]
                    controllerRef.volumetoshow = UserDefaults.standard.integer(forKey: "poolvolume")
                     self.navigationController?.pushViewController(controllerRef, animated: true)
                    break
                  default:
                      break
                      
                  }
            
          
            
        }
        if selectedSegmentIndex == 2 {
            
            switch level {
                  case 1:
                    let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                      break
                  case 2:
                    let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                      break
                  case 3:
                    let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                      break
                case 4:
                    controllerRef.didSelect = waterfeatureArray[indexPath.row]
                               controllerRef.volumetoshow =  UserDefaults.standard.integer(forKey: "watervolume")
                     self.navigationController?.pushViewController(controllerRef, animated: true)
                    break
                case 5:
                    let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    break
                case 6:
                    controllerRef.didSelect = waterfeatureArray[indexPath.row]
                               controllerRef.volumetoshow =  UserDefaults.standard.integer(forKey: "watervolume")
                     self.navigationController?.pushViewController(controllerRef, animated: true)
                    break
                  default:
                      break
                      
                  }
            
            
            
            
            
            
            
            
//            if level == 4 || level == 6 {
//            controllerRef.didSelect = waterfeatureArray[indexPath.row]
//            controllerRef.volumetoshow =  UserDefaults.standard.integer(forKey: "watervolume")
//            }else {
//                let alert = UIAlertController(title: "", message: "Your account is not allow for this feature. Please contact us.", preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
       }
//        self.navigationController?.pushViewController(controllerRef, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = optionsCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OptionsCollectionViewCell
        cell.layer.cornerRadius = 8.0
        cell.layer.borderColor = #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1)
        cell.layer.borderWidth = 1.5
        cell.nameslbl.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.nameslbl.textColor = #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1)
        
        if selectedSegmentIndex == 0 {
            cell.nameslbl.text = self.spaArray[indexPath.row]
        }
        
        if selectedSegmentIndex == 1 {
            cell.nameslbl.text = self.poolArray[indexPath.row]
        }
        
        if selectedSegmentIndex == 2 {
            cell.nameslbl.text = self.waterfeatureArray[indexPath.row]
        }
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = collectionView.cellForItem(at: indexPath) as? OptionsCollectionViewCell {
                cell.nameslbl.transform = .init(scaleX: 0.95, y: 0.95)
                cell.contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = collectionView.cellForItem(at: indexPath) as? OptionsCollectionViewCell {
                cell.nameslbl.transform = .identity
                cell.contentView.backgroundColor = .clear
            }
        }
    }
}
