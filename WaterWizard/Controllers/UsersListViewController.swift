//
//  UsersListViewController.swift
//  WaterWizard
//
//  Created by Khawar Khan on 23/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import iOSDropDown
import FirebaseDatabase
import Firebase

class UsersListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tbvu: UITableView!
    var ref: DatabaseReference!
    
    var emaildata = [String]()
    var leveldata = [String]()
    var passworddata = [String]()
    var firbaseuidarray = [String]()
    var deleteddataa = [Bool]()
    var userdeletes = false
    
   // var firebaseSnapObjectArr = [DataSnapshot]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbvu.tableFooterView = UIView()
        navigationController?.navigationBar.tintColor = .white
        self.gettingvaluesfromDB()
        self.addBackBtn()
        self.title = "All Users"
    }
    
    func addBackBtn(){
        
        let backBtn = UIBarButtonItem(image: UIImage(named: k_backBtn)?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(tappedBackBtn))
        backBtn.tintColor = #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1)
        self.navigationItem.leftBarButtonItem = backBtn
        
    }
    
    @objc func tappedBackBtn(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func addRightBarBtn(){
        
        let backBtn = UIBarButtonItem(image: UIImage(named: "arrow-point-to-right.png"), style: .done, target: self, action: #selector(tappedRightBtn))
        backBtn.tintColor = .white
        self.navigationItem.rightBarButtonItem = backBtn
        
    }
    
    @objc func tappedRightBtn(){
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emaildata.count //self.firebaseSnapObjectArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersListTableViewCell", for: indexPath) as! UsersListTableViewCell
        
        cell.emailtxtfield.isUserInteractionEnabled = false
        cell.passwordtxtfield.isUserInteractionEnabled = false
        cell.levelDropDown.isUserInteractionEnabled = false
        cell.saveChangesBtn.isHidden = true
        
//        let user = self.firebaseSnapObjectArr[indexPath.row]
//        let email = user.childSnapshot(forPath: "email").value as! String
//        let password = user.childSnapshot(forPath: "password").value as! String
//        let level = user.childSnapshot(forPath: "level").value as! String

//        cell.emailtxtfield.text = email
//        cell.passwordtxtfield.text = password
//        cell.levelDropDown.text = level
        
        cell.emailtxtfield.text = emaildata[indexPath.row]
        cell.passwordtxtfield.text = passworddata[indexPath.row]
        cell.levelDropDown.text =  leveldata[indexPath.row]
        
        cell.levelDropDown.textAlignment = .center
        cell.editBtn.tag = indexPath.row
        cell.saveChangesBtn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        cell.blockBtn.tag = indexPath.row
         cell.blockBtn.addTarget(self, action: #selector(blockBtnTapped), for: .touchUpInside)
        cell.editBtn.addTarget(self, action: #selector(editBtnTapped), for: .touchUpInside)
        cell.saveChangesBtn.addTarget(self, action: #selector(savechangesBtnTapped), for: .touchUpInside)
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnTapped), for: .touchUpInside)
               
        
        return cell
    }
     @objc func blockBtnTapped(sender: UIButton!) {
        let uid = firbaseuidarray[sender.tag]
        let alertController = UIAlertController(title: "", message: "Do you want to block this user?", preferredStyle: .alert)

               // Create the actions
               let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                   UIAlertAction in
                   NSLog("OK Pressed")
                   self.ref = Database.database().reference()
                          self.ref.child("user").child(uid).updateChildValues(["blocked" : true])
                         
                          self.emaildata.remove(at: sender.tag)
                          self.passworddata.remove(at: sender.tag)
                          self.leveldata.remove(at: sender.tag)
                          self.tbvu.reloadData()
                   self.showalertwithmessage(title: "", message: "User has been blocked")
               }
               let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                   UIAlertAction in
                   NSLog("Cancel Pressed")
               }

               // Add the actions
               alertController.addAction(okAction)
               alertController.addAction(cancelAction)

               // Present the controller
               self.present(alertController, animated: true, completion: nil)
               
        
    }
    
    @objc func editBtnTapped(sender: UIButton!) {
        
        let currentIndexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let currentCell = tbvu.cellForRow(at: currentIndexPath as IndexPath) as! UsersListTableViewCell
        currentCell.saveChangesBtn.isHidden = false
        currentCell.emailtxtfield.isUserInteractionEnabled = true
        currentCell.passwordtxtfield.isUserInteractionEnabled = true
        currentCell.levelDropDown.isUserInteractionEnabled = true
        currentCell.levelDropDown.textAlignment = .center
        currentCell.levelDropDown.optionArray = ["1", "2","3","4","5","6"]
        //Its Id Values and its optional
        
        //            currentCell.levelDropDown.text = currentCell.levelDropDown.optionArray[0]
        
        // The the Closure returns Selected Index and String
        currentCell.levelDropDown.didSelect{(selectedText , index ,id) in
            currentCell.levelDropDown.text = " \(selectedText)"
            
            
        }

    }
    @objc func savechangesBtnTapped(sender: UIButton!) {
        
        let currentIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let currentCell = tbvu.cellForRow(at: currentIndexPath as IndexPath) as! UsersListTableViewCell
        let level = currentCell.levelDropDown.text
        var level1 = 0
        level1 = Int(level!)!
        
        ref = Database.database().reference()
        ref.child("user").child(firbaseuidarray[sender.tag]).updateChildValues(["email" : currentCell.emailtxtfield.text as Any])
         ref.child("user").child(firbaseuidarray[sender.tag]).updateChildValues(["password" : currentCell.passwordtxtfield.text as Any])
        ref.child("user").child(firbaseuidarray[sender.tag]).updateChildValues(["level" : level1])
//        tbvu.reloadData()
        passworddata[sender.tag] = currentCell.passwordtxtfield.text!
        emaildata[sender.tag] = currentCell.emailtxtfield.text!
        leveldata[sender.tag] = currentCell.levelDropDown.text!
        tbvu.reloadData()
        currentCell.saveChangesBtn.isHidden = true
        showalertwithmessage(title: "", message: "Your changes have been saved")
        
        
    }
    @objc func deleteBtnTapped(sender: UIButton!) {
    
        _ = NSIndexPath(row: sender.tag, section: 0)
//        var uid = firbaseuidarray[sender.tag]
        _ = Auth.auth().currentUser
        let email = emaildata[sender.tag]
        _ = String(email)
        _ = passworddata[sender.tag]
        let uid = firbaseuidarray[sender.tag]
//        let password1 = String(password)
//        Auth.auth().signIn(withEmail: email1, password: password) { [weak self] authResult, error in
//            if error == nil{
//                let uid = user?.uid
//                self!.ref = Database.database().reference()
//                self!.ref.child("user").child(uid!).updateChildValues(["deleted" : true])
//                self!.deleteddataa[sender.tag] = true
//               // Create the alert controller
//                let alertController = UIAlertController(title: "", message: "Do you want to delete this user?", preferredStyle: .alert)
//
//               // Create the actions
//                let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
//                   UIAlertAction in
//                   NSLog("OK Pressed")
//
//
//                    user?.delete { error in
//                      if let error = error {
//                        // An error happened.
//                      } else {
//                        // Account deleted.
//                        print("account has been deleted")
//                        self!.userdeletes = true
//                        self!.emaildata.remove(at: sender.tag)
//                                       self!.passworddata.remove(at: sender.tag)
//                                       self!.leveldata.remove(at: sender.tag)
//                        self!.tbvu.reloadData()
//                        self?.deletedtrue()
//            }
//                   }
//                }
//                let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
//                    UIAlertAction in
//                    NSLog("Cancel Pressed")
//                }
//
//                // Add the actions
//                alertController.addAction(okAction)
//                alertController.addAction(cancelAction)
//
//                // Present the controller
//                self!.present(alertController, animated: true, completion: nil)
//
//                   // Present the controller
//
//
//
//
//            }
//
//
//        }
        
        
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: "Do you want to delete this user?", preferredStyle: .alert)

        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.ref = Database.database().reference()
                   self.ref.child("user").child(uid).updateChildValues(["deleted" : true])
                   self.deleteddataa[sender.tag] = true
                   self.emaildata.remove(at: sender.tag)
                   self.passworddata.remove(at: sender.tag)
                   self.leveldata.remove(at: sender.tag)
                   self.tbvu.reloadData()
            self.showalertwithmessage(title: "", message: "User has been deleted")
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }

        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)

        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
       
        
        

    }
    
    func showalertwithmessage(title:String, message: String){
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
    }
    
    func gettingvaluesfromDB(){
        
            AppGlobals.shared.showActivityIndicatory(view: self.view, containerColor: .white, loaderColor: #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1))
        
            ref = Database.database().reference()
        _ = ref.child("user").observe(.childAdded, with: {(snapshot) in
                

            
            if let childsObjectArray = snapshot.children.allObjects as? [DataSnapshot] {
                for childs in childsObjectArray {
                    if let childDict = childs.value as? Dictionary<String,Any> {
                        print(childDict)
                    }
                }
            }
            
            let deleted = snapshot.childSnapshot(forPath: "deleted").value
            let isdelete = deleted as? Bool
            let blocked = snapshot.childSnapshot(forPath: "blocked").value
            let isblocked = blocked as? Bool
            
            if isdelete == false && isblocked == false {
                
                let username = snapshot.childSnapshot(forPath: "email").value
                
                let email = username as? String
                if let actualemail = email{
                    self.emaildata.append(actualemail)
                }
                
                let deleted1 = snapshot.childSnapshot(forPath: "deleted").value
                
                let isdelete1 = deleted1 as? Bool
                if let actualdeletevalue = isdelete1{
                    self.deleteddataa.append(actualdeletevalue)
                }
                
                let level1 = snapshot.childSnapshot(forPath: "level").value
                
                let level2 = level1 as? Int
                if let actuallevel = level2 {
                    let level = String(actuallevel)
                    self.leveldata.append(level)
                }
                let password = snapshot.childSnapshot(forPath: "password").value
                
                let pass = password as? String
                if let actualpass = pass{
                    self.passworddata.append(actualpass)
                }
                let uid = snapshot.childSnapshot(forPath: "firebaseUID").value
                print(username!)
                
                let firbaseuid = uid as? String
                if let actualpass = firbaseuid {
                    DispatchQueue.main.async {
//
//                        self.firebaseSnapObjectArr = snapshot.children.allObjects as! [DataSnapshot]
                        self.firbaseuidarray.append(actualpass)
                    }
                }
                self.setuptbvu()
            }
            
        })
    
}
    func setuptbvu(){
        tbvu.delegate = self
        tbvu.dataSource = self
        tbvu.reloadData()
        AppGlobals.shared.hideActivityIndicator(view: self.view)
    }
//    method to delete from database
     func remove(parentA: String) {
      var ref = Database.database().reference()
       ref = ref.child("user").child(parentA)

        ref.removeValue { error, _ in

            print(error)
        }
    }
    
 
    
    
}


