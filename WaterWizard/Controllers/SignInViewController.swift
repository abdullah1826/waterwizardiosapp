//
//  ViewController.swift
//  WaterWizard
//
//  Created by Muhammad Abdullah on 04/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import Firebase
import ObjectMapper
import FirebaseDatabase



class SignInViewController: BaseViewController, UNUserNotificationCenterDelegate {
   var ref: DatabaseReference!

      
      
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var eyeBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton! {
        didSet{
            signInBtn.layer.cornerRadius = 8.0
        }
    }
    
    @IBOutlet weak var signUpBtn: UIButton!
    var iconClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.readingdatafromdb()
        //
        
        
        
        
    }
    func readingdatafromdb(){
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        ref.child("hennawebapp").child("user").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let username = value?["username"] as? String ?? ""
            let level = value?["level"]
            print(level)
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    
    @objc func tappedForgotPassword(_ sender: UIButton){
        
    }
    
    @IBAction func eyeAction(_ sender: Any) {
        
        if(iconClick == true) {
            passwordField.isSecureTextEntry = false
            eyeBtn.setImage(UIImage(named: "eye-visible") , for: .normal)
            
        } else {
            passwordField.isSecureTextEntry = true
            eyeBtn.setImage(UIImage(named: "eye-invisible") , for: .normal)
        }
        
        iconClick = !iconClick
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        if emailField.text == "" {
            let alert = UIAlertController(title: "", message: "Please enter email", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        } else if passwordField.text == "" {
            let alert = UIAlertController(title: "", message: "Please enter password", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if emailField.text != "" && passwordField.text != ""{
            // self.loginUser()
            self.signIn(withEmail: emailField.text!, password: passwordField.text!)
            //            signup()
            //            self.pushController(controller: HomeViewController.id, storyboard: Storyboards.Main.id)
        }
    }
    func signIn(withEmail:String, password:String){
        AppGlobals.shared.showActivityIndicatory(view: self.view, containerColor: .white, loaderColor: #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1))
        Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) {
            authResult, error in
            if error == nil {
            var currentuser = Auth.auth().currentUser
            var email = currentuser?.email
            print(email)
            var uid = currentuser?.uid
            let defaults = UserDefaults.standard
            defaults.set(email, forKey: "emailsaved")
            
            defaults.set(uid, forKey: "uidsaved")
            self.ref = Database.database().reference()
            
            
            var refHandle = self.ref.child("user").child(uid!).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let post = snapshot.value as? String
                let level1 = snapshot.childSnapshot(forPath: "level").value
                
                var level2 = level1 as? Int
                if let actuallevel = level2 {
                    defaults.set(actuallevel, forKey: "levelsaved")
                }
                
                
                
                let password1 = snapshot.childSnapshot(forPath: "password").value
                
                var pass2 = password1 as? String
                if let actualpass = pass2 {
                    defaults.set(actualpass, forKey: "passwordsaved")
                }
                
                
                
                
                AppGlobals.shared.hideActivityIndicator(view: self.view)
                NotificationCenter.default.post(name: Notification.Name("refreshtabbaritems"), object: nil)
                
                self.dismiss(animated: true, completion: nil)
                //               self.pushController(controller: HomeViewController.id, storyboard: Storyboards.Main.id)
              
            })
        }
        
            if  error != nil {
                              //                   self.updateUser(user)
                              let alert = UIAlertController(title: "", message: error?.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                              alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                              self.present(alert, animated: true, completion: nil)
                              print("error")
                              AppGlobals.shared.hideActivityIndicator(view: self.view)
                              
                              
                              
                          }
            
        }
    }
    
    func signup(){
        Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) { authResult, error in
            if error == nil{
                print("signup complete")
            }else {
                print(error?.localizedDescription)
            }
        }
        
    }
    
    
    
    //    func loginUser() {
    //
    //      //  SVProgressHUD.show(withStatus: "Please wait, trying to login...")
    //
    //        guard let email = emailField.text, !email.isEmpty  else { return }
    //        guard let password = passwordField.text, !password.isEmpty  else { return }
    //
    //        let params: [String: Any] = ["email": email, "password": password, "device_token": "Modelraksfhsfkhsqaqfhqqhoqeeqoifeqyq"]
    //
    //        self.showloader()
    //
    //        NetworkManager.shared().LOGIN(params: params, onSuccess: { (user
    //            ) in
    //            AppGlobals.removeUserData()
    //            User.loggedUser = user
    //            self.showSuccess()
    //            if #available(iOS 13.0, *) {
    //                sceneDelegate.userDidLoggedIn()
    //            } else {
    //                // Fallback on earlier versions
    //                appDelegate.userDidLoggedIn()
    //            }
    //
    //        }, onFailure: { (error) in
    //            self.showFail()
    //            let alert = UIAlertController(title: "Alert", message:error?.localizedDescription , preferredStyle: .alert)
    //            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    //            self.present(alert, animated: true, completion: nil)
    //
    //
    //        }) { (mess) in
    //            print("Login Failed")
    //            self.showFail()
    //
    //            let alert = UIAlertController(title: "Alert", message: mess, preferredStyle: .alert)
    //            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    //            self.present(alert, animated: true, completion: nil)
    //
    //        }
    //
    //    }
    
}


