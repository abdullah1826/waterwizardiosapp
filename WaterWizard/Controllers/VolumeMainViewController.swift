//
//  VolumeMainViewController.swift
//  WaterWizard
//
//  Created by Khawar Khan on 22/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import iOSDropDown
import WMSegmentControl

class VolumeMainViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var calculateVolumelbl: UILabel!
    var selectedSegmentIndex: Int = 0 { didSet{
        let volumetoshow = String(UserDefaults.standard.integer(forKey: "spavolume"))
        showingvolumelbl.text = volumetoshow + " " + "liters"
        }}
    var entervolume: Int!
    var spavolume : Int!
    var poolvolume: Int!
    var watervolume: Int!

    @IBOutlet weak var avrgHeightBtn: UIButton!
    @IBOutlet weak var measuringUnit: DropDown!
    @IBOutlet weak var widthTxtField: UITextField!
    @IBOutlet weak var lengthTxtfield: UITextField!
    @IBOutlet weak var showingvolumelbl: UILabel!
    @IBOutlet weak var saveVolumeBtn: UIButton!
    @IBOutlet weak var entervolumeTxtfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationbarLogo()
        
        self.setSegmentController()
        self.setupmeasuringunitdropdown()
        
        saveVolumeBtn.layer.cornerRadius = 5
        saveVolumeBtn.layer.borderWidth = 1
        saveVolumeBtn.layer.borderColor = UIColor().colorForHax("#035DC2").cgColor
        
        lengthTxtfield.placeholder = "Length"
        widthTxtField.placeholder = "Width"
        entervolumeTxtfield.placeholder = "Enter Your Volume"
        lengthTxtfield.delegate = self
        widthTxtField.delegate = self
        entervolumeTxtfield.delegate = self
        lengthTxtfield.keyboardType = UIKeyboardType.numberPad
        widthTxtField.keyboardType = UIKeyboardType.numberPad
        entervolumeTxtfield.keyboardType = UIKeyboardType.numberPad
                
        //adding notification to get value of average height
          NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("sendavgheight"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
           if let avgheight = notification.userInfo?["averageheight"] as? Int {
            avrgHeightBtn.setTitle(String(avgheight), for: .normal)
            avrgHeightBtn.setTitleColor(.black, for: .normal)
           }
       }
    
    @IBAction func averageheightBtnTpd(_ sender: Any) {
       let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AverageHeightViewController") as? AverageHeightViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func navigationbarLogo(){
           
           let image = UIImage(named:"banner.png")
           let imageView = UIImageView(image: image)
           
           imageView.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
           imageView.contentMode = .scaleAspectFit
           imageView.clipsToBounds = true
           self.navigationItem.titleView = imageView
           
       }
    
    @IBAction func saveVolumeBtnTpd(_ sender: Any) {
        if entervolumeTxtfield.text != ""{
            if selectedSegmentIndex == 0{
                let defaults = UserDefaults.standard
                 spavolume = Int(entervolumeTxtfield.text!)
                showingvolumelbl.text = entervolumeTxtfield.text! + " " + "liters"
                defaults.set(spavolume, forKey: "spavolume")
                entervolumeTxtfield.text = ""
                
            }
           
            if selectedSegmentIndex == 2{
                let defaults = UserDefaults.standard
                showingvolumelbl.text = entervolumeTxtfield.text! + " " + "liters"
                 poolvolume = Int(entervolumeTxtfield.text!)
                               defaults.set(poolvolume, forKey: "poolvolume")
                entervolumeTxtfield.text = ""
                   
            }
            if selectedSegmentIndex == 3{
                let defaults = UserDefaults.standard
                               watervolume = Int(entervolumeTxtfield.text!)
                showingvolumelbl.text = entervolumeTxtfield.text! + " " + "liters"
                defaults.set(watervolume, forKey: "watervolume")
                entervolumeTxtfield.text = ""
                 
            }
        }
        else {
            let alert = UIAlertController(title: "", message: "Please enter value", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBOutlet weak var segmentController: WMSegment!
    
    @IBAction func segmentcontrollertapped(_ sender: WMSegment) {
        selectedSegmentIndex = sender.selectedSegmentIndex
          
            switch sender.selectedSegmentIndex {
                case 0:
                    calculateVolumelbl.text = "Calculate Spa Volume"
                    let volumetoshow = String(UserDefaults.standard.integer(forKey: "spavolume"))
                    showingvolumelbl.text = volumetoshow + " " + "liters"
                break
                case 1:
                    calculateVolumelbl.text = "Calculate Site Volume"
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SiteViewController") as? SiteViewController
                          self.navigationController?.pushViewController(vc!, animated: true)
                               
                break
                case 2:
                    calculateVolumelbl.text = "Calculate Pool Volume"
                    let volumetoshow = String(UserDefaults.standard.integer(forKey: "poolvolume"))
                    showingvolumelbl.text = volumetoshow + " " + "liters"
                break
                case 3:
                    calculateVolumelbl.text = "Calculate Water Feature Volume"
                    let volumetoshow = String(UserDefaults.standard.integer(forKey: "watervolume"))
                    showingvolumelbl.text = volumetoshow + " " + "liters"
                break
            default:
                break

            }

        
        
    }
    func setupmeasuringunitdropdown(){
        // The list of array to display. Can be changed dynamically
        measuringUnit.optionArray = ["Meters", "Feet"]
        //Its Id Values and its optional
        measuringUnit.optionIds = [1,23,54,22]
        measuringUnit.text = measuringUnit.optionArray[0]
       measuringUnit.arrowColor = UIColor.black
        measuringUnit.selectedRowColor = UIColor().colorForHax("#035DC2")
        measuringUnit.arrowColor = UIColor().colorForHax("#035DC2")
      
        // The the Closure returns Selected Index and String
        measuringUnit.didSelect{(selectedText , index ,id) in
        self.measuringUnit.text = "\(selectedText)"
            self.measuringUnit.textColor = UIColor().colorForHax("#035DC2")
        
            
        }
    }
    
    
   func setSegmentController() {
          segmentController.type = .normal
          segmentController.selectorType = .bottomBar
          segmentController.selectorColor = Utilites.appColor
          segmentController.normalFont = UIFont(name: "ChalkboardSE-Regular", size: 12)!
          segmentController.SelectedFont = UIFont(name: "ChalkboardSE-Bold", size: 12)!
      }
    
    @IBAction func lengthTxtfieldTpd(_ sender: Any) {
    }
    @IBAction func widthtxtfldTpd(_ sender: Any) {
    }
    
    @IBAction func enterVolumeTxtfieldTpd(_ sender: Any) {
    }
    
    
    
    

}
extension UIColor {
    
    func colorForHax(_ rgba:String)->UIColor{
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        if rgba.hasPrefix("#") {
            let index   = rgba.index(rgba.startIndex, offsetBy: 1)
            let hex     = rgba.substring(from: index)
            let scanner = Scanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            if scanner.scanHexInt64(&hexValue) {
                switch (hex.count) {
                case 3:
                    red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                    green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                    blue  = CGFloat(hexValue & 0x00F)              / 15.0
                    break
                case 4:
                    red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                    green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                    blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                    alpha = CGFloat(hexValue & 0x000F)             / 15.0
                    break
                case 6:
                    red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
                    break
                case 8:
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                    break
                default:
                    print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
                    break
                }
            } else {
                print("Scan hex error")
            }
        } else {
            print("Invalid RGB string, missing '#' as prefix", terminator: "")
        }
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    
}
