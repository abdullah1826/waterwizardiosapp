//
//  AdminViewController.swift
//  WaterWizard
//
//  Created by Khawar Khan on 23/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import Firebase
import ObjectMapper
import FirebaseDatabase


class AdminViewController: BaseViewController {
    
     var window: UIWindow?
   var ref: DatabaseReference!
    @IBOutlet weak var logoutBtn: UIButton!
    
    @IBOutlet weak var createSiteBtn: UIButton!
    @IBOutlet weak var createnewuserBtn: UIButton!
    
    @IBOutlet weak var seeUsersBtn: UIButton!
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var eyeBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton! {
        didSet{
            signInBtn.layer.cornerRadius = 8.0
        }
    }


    var iconClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createnewuserBtn.layer.cornerRadius = 5
        createnewuserBtn.layer.borderWidth = 1
         createnewuserBtn.layer.borderColor = UIColor().colorForHax("#035DC2").cgColor
        logoutBtn.layer.cornerRadius = 5
        logoutBtn.layer.borderWidth = 1
         logoutBtn.layer.borderColor = UIColor().colorForHax("#035DC2").cgColor
        createSiteBtn.layer.cornerRadius = 5
        createSiteBtn.layer.borderWidth = 1
         createSiteBtn.layer.borderColor = UIColor().colorForHax("#035DC2").cgColor
        seeUsersBtn.layer.cornerRadius = 5
              seeUsersBtn.layer.borderWidth = 1
               seeUsersBtn.layer.borderColor = UIColor().colorForHax("#035DC2").cgColor
        
        
        let level = UserDefaults.standard.integer(forKey: "levelsaved")
        let email1 = UserDefaults.standard.string(forKey: "emailsaved")
        if email1 == "andrewschofield@waterwizard.com"{
            createSiteBtn.isHidden = false
            createnewuserBtn.isHidden = false
            seeUsersBtn.isHidden = false
            logoutBtn.isHidden = false
        }else{
            seeUsersBtn.isHidden = true
            createSiteBtn.isHidden = true
            createnewuserBtn.setTitle("Update", for: .normal)
        }
        
//        self.readingdatafromdb()
let email = UserDefaults.standard.string(forKey: "emailsaved")
        self.emailField.text = email
        let password = UserDefaults.standard.string(forKey: "passwordsaved")
        self.passwordField.text = password
        
//        addBackBtn()
        addRightBarBtn()
        
        
    }
   
      
    
    @IBAction func seeuserBtnTapped(_ sender: Any) {

        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UsersListViewController") as? UsersListViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func logoutBtnTapped(_ sender: Any) {
        userDidLoggedOut()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    @IBAction func createuserBtnTapped(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
               let balanceViewController = storyBoard.instantiateViewController(withIdentifier: "createusernavigation") as! UINavigationController
               self.present(balanceViewController, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func createsiteBtnTapped(_ sender: Any) {
       
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "createsitecontroller") as? CeateSiteViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @objc func tappedForgotPassword(_ sender: UIButton){
        
    }
    
    @IBAction func eyeAction(_ sender: Any) {
        
        if(iconClick == true) {
            passwordField.isSecureTextEntry = false
            eyeBtn.setImage(UIImage(named: "eye-visible") , for: .normal)
            
        } else {
            passwordField.isSecureTextEntry = true
            eyeBtn.setImage(UIImage(named: "eye-invisible") , for: .normal)
        }
        
        iconClick = !iconClick
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        if emailField.text == "" {
            return
        } else if passwordField.text == "" {
            return
        }
        
        if emailField.text != "" && passwordField.text != ""{
           // self.loginUser()
            self.signIn(withEmail: emailField.text!, password: passwordField.text!)
//            signup()
//            self.pushController(controller: HomeViewController.id, storyboard: Storyboards.Main.id)
        }
    }
    func signIn(withEmail:String, password:String){
           Auth.auth().signIn(withEmail: withEmail, password: password) {
               authResult, error in
              
               if error == nil{
//                   self.updateUser(user)
                print("error")
               }
           }
       }
    func signup(){
        Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) { authResult, error in
            if error == nil{
                print("signup complete")
            }else {
                print(error?.localizedDescription)
            }
        }

    }
    func userDidLoggedOut() {
           
//           for view in (window?.subviews)! {
//               view.removeFromSuperview()
//           }
           
           
           self.resetDefaults()
        self.removedefaults()
           
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(withIdentifier: "signinnavigation")
        self.present(vc, animated: true)
       }
       func resetDefaults() {
           let defaults = UserDefaults.standard
           let dictionary = defaults.dictionaryRepresentation()
           dictionary.keys.forEach { key in
               defaults.removeObject(forKey: key)
           }
       }

    func removedefaults(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
    
    
    
}
