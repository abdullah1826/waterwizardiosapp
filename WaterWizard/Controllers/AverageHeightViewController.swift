//
//  AverageHeightViewController.swift
//  WaterWizard
//
//  Created by Khawar Khan on 24/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit

class AverageHeightViewController: UIViewController {

    @IBOutlet weak var deepPointTxtfield: UITextField!
    @IBOutlet weak var shallowPointTxtfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        deepPointTxtfield.keyboardType = UIKeyboardType.numbersAndPunctuation
        shallowPointTxtfield.keyboardType = UIKeyboardType.numbersAndPunctuation
        addBackBtn()

        // Do any additional setup after loading the view.
    }
    func addBackBtn(){
                        
                let backBtn = UIBarButtonItem(image: UIImage(named: k_backBtn)?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(tappedBackBtn))
                    backBtn.tintColor = #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1)
                    self.navigationItem.leftBarButtonItem = backBtn

            }
            
            @objc func tappedBackBtn(){
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }

    @IBAction func averageHeightBtnTapped(_ sender: Any) {
        if deepPointTxtfield.text == nil || shallowPointTxtfield == nil{
            let alert = UIAlertController(title: "", message: "Please inset value.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        if deepPointTxtfield.text != "" && shallowPointTxtfield.text != ""{
        var deeptxtfield = deepPointTxtfield.text
        var shallowtxtfield = shallowPointTxtfield.text
        var value1 = Int(deeptxtfield!)
        var value2 = Int(shallowtxtfield!)
            var avgHeight = (value1! + value2!) / 2
            
            let AvgHeight:[String: Int] = ["averageheight": avgHeight]

            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sendavgheight"), object: nil, userInfo: AvgHeight)
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
           
        }else {
            
        }
    }
    
}
