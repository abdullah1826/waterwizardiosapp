//
//  CeateSiteViewController.swift
//  WaterWizard
//
//  Created by Khawar Khan on 13/07/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import iOSDropDown
import Firebase

class CeateSiteViewController: UIViewController {
    var spavolume : Int!
    var poolvolume: Int!
    var watervolume: Int!
    var lifeguardBtnTap = false
    var maintenanceBtnTap = false
    var serviceBtnTap = false
    var recBtnTap = false
    @IBOutlet weak var createSiteBtn: UIButton!
    
    @IBOutlet weak var selectTypeDropDown: DropDown!
    
    @IBOutlet weak var recImgVu: UIImageView!
    @IBOutlet weak var scrollVu: UIScrollView!
    
    @IBOutlet weak var scrollContentVu: UIView!
    
    @IBOutlet weak var siteNameTxtfield: UITextField!
    
    @IBOutlet weak var measuringUnitDropDown: DropDown!
    @IBOutlet weak var siteAddressTxtfield: UITextField!
    
    @IBOutlet weak var lifeguardImgVu: UIImageView!
    
    @IBOutlet weak var maintenanceImgVu: UIImageView!
    
    @IBOutlet weak var serviceImgVu: UIImageView!
    
    @IBOutlet weak var typeDropDown: DropDown!
    
    
    @IBOutlet weak var lengthTxtfield: UITextField!
    @IBOutlet weak var widthTxtfield: UITextField!
    
    @IBOutlet weak var showingVolumeLbl: UILabel!
    
    @IBOutlet weak var enterVolumeTxtfield: UITextField!
    
    @IBOutlet weak var saveVolumeBtn: UIButton!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setuptypedropdown()
        setupmeasuringunitdropdown()
        setupselecttypedropdown()
        saveVolumeBtn.layer.cornerRadius = 5
              saveVolumeBtn.layer.borderWidth = 1
              saveVolumeBtn.layer.borderColor = UIColor().colorForHax("#035DC2").cgColor
        createSiteBtn.layer.cornerRadius = 5
                     createSiteBtn.layer.borderWidth = 1
                     createSiteBtn.layer.borderColor = UIColor().colorForHax("#035DC2").cgColor
          addBackBtn()
              self.title = "Create New Site"
        // Do any additional setup after loading the view.
    }
    func setupmeasuringunitdropdown(){
        // The list of array to display. Can be changed dynamically
        measuringUnitDropDown.optionArray = ["Meters", "Feet"]
        //Its Id Values and its optional
       
        measuringUnitDropDown.text = measuringUnitDropDown.optionArray[0]
        measuringUnitDropDown.textColor = UIColor.black
        measuringUnitDropDown.selectedRowColor = UIColor().colorForHax("#035DC2")
        measuringUnitDropDown.arrowColor = UIColor.black
        
       
        
      
        // The the Closure returns Selected Index and String
        measuringUnitDropDown.didSelect{(selectedText , index ,id) in
        
        self.measuringUnitDropDown.text = "\(selectedText)"
            self.measuringUnitDropDown.textColor = UIColor().colorForHax("#035DC2")
           
            
            
        }
    }
    func setupselecttypedropdown(){
           // The list of array to display. Can be changed dynamically
           selectTypeDropDown.optionArray = ["Pool", "Warer Feature"]
           //Its Id Values and its optional
          
           selectTypeDropDown.text = "Select Type"
           selectTypeDropDown.textColor = UIColor.black
           selectTypeDropDown.selectedRowColor = UIColor().colorForHax("#035DC2")
           selectTypeDropDown.arrowColor = UIColor.black
         
           // The the Closure returns Selected Index and String
           selectTypeDropDown.didSelect{(selectedText , index ,id) in
           self.selectTypeDropDown.text = "\(selectedText)"
               self.selectTypeDropDown.textColor = UIColor().colorForHax("#035DC2")
            
               
               
           }
       }
    func setuptypedropdown(){
          // The list of array to display. Can be changed dynamically
          typeDropDown.optionArray = ["Pool", "Spa","Water Feature"]
          //Its Id Values and its optional
       
          typeDropDown.text = " Select"
          typeDropDown.textColor = UIColor.black
        typeDropDown.arrowColor = UIColor.black
                 typeDropDown.selectedRowColor = UIColor().colorForHax("#035DC2")
          // The the Closure returns Selected Index and String
          typeDropDown.didSelect{(selectedText , index ,id) in
          self.typeDropDown.text = "\(selectedText)"
              self.typeDropDown.textColor = UIColor().colorForHax("#035DC2")
           
              
              
          }
      }
    func addBackBtn(){
           
           let backBtn = UIBarButtonItem(image: UIImage(named: "fill9")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(tappedBackBtn))
           backBtn.tintColor = #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1)
           self.navigationItem.leftBarButtonItem = backBtn
           
       }
       
       @objc func tappedBackBtn(){
           self.navigationController?.popViewController(animated: true)
           self.dismiss(animated: true, completion: nil)
       }
    func showalert(message: String){
            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
                                      alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
               self.present(alert, animated: true, completion: nil)
       }
    
    
    
    
    @IBAction func recBtnTapped(_ sender: Any) {
        if recBtnTap == false{
                  recBtnTap = true
                  recImgVu.image = UIImage(named: "checkbox")
                  maintenanceImgVu.image = UIImage(named: "frame")
                  serviceImgVu.image = UIImage(named: "frame")
              }else{
                  recBtnTap = false
                  recImgVu.image = UIImage(named: "frame")
              }
    }
    
    @IBAction func lifeguardBtnTapped(_ sender: Any) {
        if lifeguardBtnTap == false{
            lifeguardBtnTap = true
            lifeguardImgVu.image = UIImage(named: "checkbox")
            maintenanceImgVu.image = UIImage(named: "frame")
            serviceImgVu.image = UIImage(named: "frame")
        }else{
            lifeguardBtnTap = false
            lifeguardImgVu.image = UIImage(named: "frame")
        }
        
    }
    
    @IBAction func maintenanceBtnTapped(_ sender: Any) {
        if maintenanceBtnTap == false{
                   maintenanceBtnTap = true
                   maintenanceImgVu.image = UIImage(named: "checkbox")
                   lifeguardImgVu.image = UIImage(named: "frame")
                   serviceImgVu.image = UIImage(named: "frame")
               }else{
                   maintenanceBtnTap = false
                   maintenanceImgVu.image = UIImage(named: "frame")
               }
    }
    @IBAction func serviceBtnTapped(_ sender: Any) {
        if serviceBtnTap == false{
                          serviceBtnTap = true
                          serviceImgVu.image = UIImage(named: "checkbox")
                          lifeguardImgVu.image = UIImage(named: "frame")
                          maintenanceImgVu.image = UIImage(named: "frame")
                      }else{
                          serviceBtnTap = false
                          serviceImgVu.image = UIImage(named: "frame")
                      }
    
    }
    
    @IBAction func averageHeightBtnTapped(_ sender: Any) {
    }
    
    @IBAction func saveVolumeBtnTapped(_ sender: Any) {
        if siteAddressTxtfield.text == ""{
            showalert(message: "Please enter address")
            return
        }
        if typeDropDown.text == "Select"{
            showalert(message: "Please select type")
            return
        }
        if enterVolumeTxtfield.text != ""{
            if typeDropDown.text == "Pool"{
                let defaults = UserDefaults.standard
                poolvolume = Int(enterVolumeTxtfield.text!)
                showingVolumeLbl.text = enterVolumeTxtfield.text! + " " + "liters"
                defaults.set(poolvolume, forKey: "poolvolume")
                enterVolumeTxtfield.text = ""
                showalert(message: "Volume has been saved")
                return
                
            }
            if typeDropDown.text == "Spa"{
                let defaults = UserDefaults.standard
                spavolume = Int(enterVolumeTxtfield.text!)
                showingVolumeLbl.text = enterVolumeTxtfield.text! + " " + "liters"
                defaults.set(spavolume, forKey: "spavolume")
                enterVolumeTxtfield.text = ""
                showalert(message: "Volume has been saved")
                               return
                               
                
            }
            if typeDropDown.text == "Water Feature"{
                let defaults = UserDefaults.standard
                watervolume = Int(enterVolumeTxtfield.text!)
                showingVolumeLbl.text = enterVolumeTxtfield.text! + " " + "liters"
                defaults.set(watervolume, forKey: "watervolume")
                enterVolumeTxtfield.text = ""
                showalert(message: "Volume has been saved")
                               return
                               
                
            }
            showalert(message: "Please select type")
            
        }else {
            showalert(message: "Please enter volume")
        }
        
    }
    
    @IBAction func createSiteBtnTapped(_ sender: Any) {
        if siteNameTxtfield.text == ""{
            showalert(message: "Please eneter site name")
            return
        }
        if siteAddressTxtfield.text == ""{
            showalert(message: "Please enter site address")
            return
        }
        if typeDropDown.text == "Select"{
            showalert(message: "Please select type")
            return
        }
        if showingVolumeLbl.text == ""{
            showalert(message: "Please enter volume")
            return
        }
        createSite()
    }
    
    
    
    
    func createSite(){
        let ref = Database.database().reference()
        ref.child("sites").child(siteNameTxtfield.text!).child("name").setValue(self.siteNameTxtfield.text)
        ref.child("sites").child(siteNameTxtfield.text!).child("firebaseUID").setValue(self.siteNameTxtfield.text)
        ref.child("sites").child(siteNameTxtfield.text!).child("deleted").setValue(false)
        ref.child("sites").child(siteNameTxtfield.text!).child("address").setValue(self.siteAddressTxtfield.text)
        ref.child("sites").child(siteNameTxtfield.text!).child("siteType").setValue(self.typeDropDown.text)
        let fullName    = showingVolumeLbl.text
               let fullNameArr = fullName!.components(separatedBy: " ")

               let volumetosave    = fullNameArr[0]
//        let volumetosave = Int(name)
        ref.child("sites").child(siteNameTxtfield.text!).child("volume").setValue(volumetosave)
        showalert(message: "New Site has been created")
        
        
    }
    
    
    
    
}
