//
//  SiteTableViewCell.swift
//  WaterWizard
//
//  Created by Khawar Khan on 15/07/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import iOSDropDown

class SiteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var savechangesBtn: UIButton!
    
    @IBOutlet weak var blockBtn: UIButton!
    
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var sitenameTxtfield: UITextField!
    
    @IBOutlet weak var sitetypeDropDown: DropDown!
    
    @IBOutlet weak var volumeTxtfield: UITextField!
    @IBOutlet weak var siteaddressTxtfield: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
