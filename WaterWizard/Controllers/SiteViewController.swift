//
//  SiteViewController.swift
//  WaterWizard
//
//  Created by Khawar Khan on 25/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import iOSDropDown
import Firebase

class SiteViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tbvu: UITableView!
    
    @IBOutlet weak var searchTxtField: UITextField!
    
    @IBOutlet weak var dropdown: DropDown!
     var ref: DatabaseReference!
    var sitenamedata = [String]()
       var sitetypedata = [String]()
       var siteaddressdata = [String]()
       var firbaseuidarray = [String]()
     var childbasearray = [String]()
       var deleteddataa = [Bool]()
    var volumedata = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupdropdown()
        gettingValuesfromDB()
        // Do any additional setup after loading the view.
    }
    
    func setupdropdown(){
           // The list of array to display. Can be changed dynamically
           dropdown.optionArray = ["A-Z","1-10","10-1"]
           //Its Id Values and its optional
//           dropdown.optionIds = ["A-Z","1-10","10-1"]
           dropdown.text = dropdown.optionArray[0]
         
           // The the Closure returns Selected Index and String
           dropdown.didSelect{(selectedText , index ,id) in
           self.dropdown.text = "Selected String: \(selectedText) \n index: \(index)"
           }
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sitenamedata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "SiteTableViewCell", for: indexPath) as! SiteTableViewCell
        cell.sitenameTxtfield.text = sitenamedata[indexPath.row]
        cell.siteaddressTxtfield.text = siteaddressdata[indexPath.row]
        cell.sitetypeDropDown.text = sitetypedata[indexPath.row]
        cell.volumeTxtfield.text = volumedata[indexPath.row]
        cell.volumeTxtfield.isUserInteractionEnabled = false
        cell.sitenameTxtfield.isUserInteractionEnabled = false
        cell.siteaddressTxtfield.isUserInteractionEnabled = false
        cell.sitetypeDropDown.isUserInteractionEnabled = false
        cell.savechangesBtn.isHidden = true
        cell.editBtn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        cell.savechangesBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(editBtnTapped), for: .touchUpInside)
              cell.savechangesBtn.addTarget(self, action: #selector(savechangesBtnTapped), for: .touchUpInside)
              cell.deleteBtn.addTarget(self, action: #selector(deleteBtnTapped), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func setuptbvu(){
           tbvu.delegate = self
           tbvu.dataSource = self
           tbvu.reloadData()
           AppGlobals.shared.hideActivityIndicator(view: self.view)
       }
    func showalertwithmessage(title:String, message: String){
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                                      alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
               self.present(alert, animated: true, completion: nil)
       }
    @objc func editBtnTapped(sender: UIButton!) {
           
           let currentIndexPath = NSIndexPath(row: sender.tag, section: 0)
           
           let currentCell = tbvu.cellForRow(at: currentIndexPath as IndexPath) as! SiteTableViewCell
           currentCell.savechangesBtn.isHidden = false
           currentCell.sitenameTxtfield.isUserInteractionEnabled = true
           currentCell.siteaddressTxtfield.isUserInteractionEnabled = true
           currentCell.volumeTxtfield.isUserInteractionEnabled = true
        currentCell.sitetypeDropDown.isUserInteractionEnabled = true
           currentCell.sitetypeDropDown.textAlignment = .center
           currentCell.sitetypeDropDown.optionArray = ["Pool","Spa"]
           //Its Id Values and its optional
           
           //            currentCell.levelDropDown.text = currentCell.levelDropDown.optionArray[0]
           
           // The the Closure returns Selected Index and String
           currentCell.sitetypeDropDown.didSelect{(selectedText , index ,id) in
               currentCell.sitetypeDropDown.text = " \(selectedText)"
               
               
           }

       }
    @objc func savechangesBtnTapped(sender: UIButton!) {
        
        let currentIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let currentCell = tbvu.cellForRow(at: currentIndexPath as IndexPath) as! SiteTableViewCell
        let siteType = currentCell.sitetypeDropDown.text
        
        ref = Database.database().reference()
        ref.child("sites").child(firbaseuidarray[sender.tag]).updateChildValues(["name" : currentCell.sitenameTxtfield.text as Any])
        ref.child("sites").child(firbaseuidarray[sender.tag]).updateChildValues(["address" : currentCell.siteaddressTxtfield.text as Any])
         ref.child("sites").child(firbaseuidarray[sender.tag]).updateChildValues(["siteType" : siteType])
        //        tbvu.reloadData()
        sitenamedata[sender.tag] = currentCell.sitenameTxtfield.text!
        siteaddressdata[sender.tag] = currentCell.siteaddressTxtfield.text!
        sitetypedata[sender.tag] = currentCell.sitetypeDropDown.text!
        volumedata[sender.tag] = currentCell.volumeTxtfield.text!
        
        tbvu.reloadData()
        currentCell.savechangesBtn.isHidden = true
        showalertwithmessage(title: "", message: "Your changes have been saved")
        
        
    }
    @objc func deleteBtnTapped(sender: UIButton!) {
        let alertController = UIAlertController(title: "", message: "Do you want to delete this user?", preferredStyle: .alert)

            // Create the actions
            let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                self.ref = Database.database().reference()
                self.ref.child("sites").child(self.firbaseuidarray[sender.tag]).updateChildValues(["deleted" : true])
                       self.deleteddataa[sender.tag] = true
                       self.sitenamedata.remove(at: sender.tag)
                       self.siteaddressdata.remove(at: sender.tag)
                       self.sitetypedata.remove(at: sender.tag)
                 self.volumedata.remove(at: sender.tag)
                       self.tbvu.reloadData()
                self.showalertwithmessage(title: "", message: "It has been deleted")
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }

            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)

            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
    
    func gettingValuesfromDB(){
            
                AppGlobals.shared.showActivityIndicatory(view: self.view, containerColor: .white, loaderColor: #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1))
            
                ref = Database.database().reference()
            ref.child("sites").observe(.childAdded, with: {(snapshot) in
                    

                
                let deleted = snapshot.childSnapshot(forPath: "deleted").value
                let isdelete = deleted as? Bool
               
                if isdelete == false{
                    let sitename = snapshot.childSnapshot(forPath: "name").value
                                       
                                       let name = sitename as? String
                                       if let actualsitename = name{
                                           self.sitenamedata.append(actualsitename)
                                       }
                    
                  let uid = snapshot.childSnapshot(forPath: "firebaseUID").value
                              
                                 
                                 let firbaseuid = uid as? String
                                 if let actualpass = firbaseuid {
                                     DispatchQueue.main.async {
                 //
                 //                        self.firebaseSnapObjectArr = snapshot.children.allObjects as! [DataSnapshot]
                                         self.firbaseuidarray.append(actualpass)
                                     }
                                 }
                    let deleted1 = snapshot.childSnapshot(forPath: "deleted").value
                    
                    let isdelete1 = deleted1 as? Bool
                    if let actualdeletevalue = isdelete1{
                        self.deleteddataa.append(actualdeletevalue)
                    }
                    
                    let sitetype = snapshot.childSnapshot(forPath: "siteType").value
                    
                    let sitetype1 = sitetype as? String
                    if let actualsitetype = sitetype1 {
                       
                        self.sitetypedata.append(actualsitetype)
                    }
                    let volume1 = snapshot.childSnapshot(forPath: "volume").value
                    
                    let volume2 = volume1 as? String
                    if let actualvolume = volume2 {
                        let volume = String(actualvolume)
                        self.volumedata.append(volume)
                    }
                    let address = snapshot.childSnapshot(forPath: "address").value
                    
                    let addres = address as? String
                    if let actualaddress = addres{
                        self.siteaddressdata.append(actualaddress)
                    }
//                    let uid = snapshot.childSnapshot(forPath: "firebaseUID").value
//                    
//                    
//                    let firbaseuid = uid as? String
//                    if let actualpass = firbaseuid {
//                        DispatchQueue.main.async {
//   
//                            self.firbaseuidarray.append(actualpass)
//                        }
//                    }
                    self.setuptbvu()
                }
                
            })
        
    }
    

}
