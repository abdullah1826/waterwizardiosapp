//
//  UsersListTableViewCell.swift
//  WaterWizard
//
//  Created by Khawar Khan on 23/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import iOSDropDown

class UsersListTableViewCell: UITableViewCell {
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var saveChangesBtn: UIButton!
    @IBOutlet weak var blockBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var levelDropDown: DropDown!
    @IBOutlet weak var passwordtxtfield: UITextField!
    
    @IBOutlet weak var emailtxtfield: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
