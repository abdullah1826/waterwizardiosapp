//
//  CreateNewUserViewController.swift
//  WaterWizard
//
//  Created by Khawar Khan on 25/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase
import iOSDropDown

class CreateNewUserViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTxtfield: UITextField!
    @IBOutlet weak var passwordTxtfield: UITextField!
   
    @IBOutlet weak var levelDescriptionLbl: UILabel!
    @IBOutlet weak var levelDropDown: DropDown!
    @IBOutlet weak var createaccountBtn: UIButton!
    @IBOutlet weak var eyeBtn: UIButton!
    
    var iconClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBackBtn()
       setupLeveldropdown()
        let level = UserDefaults.standard.integer(forKey: "levelsaved")
        let password = UserDefaults.standard.string(forKey: "passwordsaved")
        let email = UserDefaults.standard.string(forKey: "emailsaved")
        
        if email != "andrewschofield@waterwizard.com" {
            
            emailTxtfield.isUserInteractionEnabled = false
            levelDropDown.isUserInteractionEnabled = false
            createaccountBtn.setTitle("Update Password", for: .normal)
            emailTxtfield.text = email
            passwordTxtfield.text = password
            levelDropDown.text = String(level)
            levelDescriptionLbl.text = ""
            
        }else{
            if levelDropDown.text == "Select"{
                levelDescriptionLbl.text = "Please select level"
            }
            if levelDropDown.text == "1"{
                levelDescriptionLbl.text = "Level 1 will let user to access the PH and Chlorine features of Pool only"
            }
            if levelDropDown.text == "2"{
                           levelDescriptionLbl.text = "Level 2 will let user to access the all features of Pool only"
                       }
            if levelDropDown.text == "3"{
                levelDescriptionLbl.text = "Level 3 will let user to access the all features of SPA only"
            }
            if levelDropDown.text == "4"{
                           levelDescriptionLbl.text = "Level 4 will let user to access the all features of Water Feature only"
                       }
            if levelDropDown.text == "5"{
                           levelDescriptionLbl.text = "Level 5 will let user to access the PH,Chlorine,Alkalinity,Neutralizier and Cya feathures of Pool and SPA"
                       }
            if levelDropDown.text == "6"{
                           levelDescriptionLbl.text = "Level 3 will let user to access the all features of POOL, SPA and Water Feature"
                       }
            
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    func addBackBtn(){
        
        let backBtn = UIBarButtonItem(image: UIImage(named: "down-arrow")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(tappedBackBtn))
        backBtn.tintColor = #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1)
        self.navigationItem.leftBarButtonItem = backBtn
        
    }
    
    @objc func tappedBackBtn(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
     func setupLeveldropdown(){
            
            levelDropDown.optionArray = ["1","2","3","4","5","6"]
            
            levelDropDown.textColor = UIColor().colorForHax("#035DC2")
            levelDropDown.text = "Select"
            levelDropDown.selectedRowColor = UIColor.blue
            
            // The the Closure returns Selected Index and String
            levelDropDown.didSelect{(selectedText , index ,id) in
                self.levelDropDown.text = "Selected String: \(selectedText) \n index: \(index)"
                self.levelDropDown.textColor = UIColor.black
    //            self.selectbrandDropdown.backgroundColor = UIColor().colorForHax("#FFCC00")
                if self.levelDropDown.text == "Select"{
                    self.levelDescriptionLbl.text = "Please select level"
                           }
                if selectedText == "1"{
                    self.levelDescriptionLbl.text = "Level 1 will let user to access the PH and Chlorine features of Pool only"
                           }
                if selectedText == "2"{
                    self.levelDescriptionLbl.text = "Level 2 will let user to access the all features of Pool only"
                                      }
                if selectedText == "3"{
                    self.levelDescriptionLbl.text = "Level 3 will let user to access the all features of SPA only"
                           }
                if selectedText == "4"{
                    self.levelDescriptionLbl.text = "Level 4 will let user to access the all features of Water Feature only"
                                      }
                if selectedText == "5"{
                    self.levelDescriptionLbl.text = "Level 5 will let user to access the PH,Chlorine,Alkalinity,Neutralizier and Cya feathures of Pool and SPA"
                                      }
                if selectedText == "6"{
                    self.levelDescriptionLbl.text = "Level 6 will let user to access the all features of POOL, SPA and Water Feature"
                                      }
            }
        }
    
    @IBAction func createaccountBtnTapped(_ sender: Any) {
        let email1 = UserDefaults.standard.string(forKey: "emailsaved")
               if email1 == "andrewschofield@waterwizard.com"{
            if emailTxtfield.text == "" || passwordTxtfield.text == "" || levelDropDown.text == "Select" {
                let alert = UIAlertController(title: "", message: "Field is empty. Please insert data.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            if emailTxtfield.text != "" && passwordTxtfield.text != "" && levelDropDown.text != "Select"{
                 AppGlobals.shared.showActivityIndicatory(view: self.view, containerColor: .white, loaderColor: #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1))
                signup()
            }
        } else{
            AppGlobals.shared.showActivityIndicatory(view: self.view, containerColor: .white, loaderColor: #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1))
            let ref = Database.database().reference()
            let uid = UserDefaults.standard.string(forKey: "uidsaved")
            ref.child("user").child(uid!).updateChildValues(["password" : passwordTxtfield.text])
            let defaults = UserDefaults.standard
            defaults.set(passwordTxtfield.text, forKey: "passwordsaved")
             AppGlobals.shared.hideActivityIndicator(view: self.view)
            let alert = UIAlertController(title: "", message: "Your Password has been updated", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func signup(){
        AppGlobals.shared.showActivityIndicatory(view: self.view, containerColor: .white, loaderColor: #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1))
        Auth.auth().createUser(withEmail: emailTxtfield.text!, password: passwordTxtfield.text!) { authResult, error in
            if error == nil{
                var uid = authResult?.user.uid
                
                let ref = Database.database().reference()
                print("signup complete")
                ref.child("user").child(uid!).child("email").setValue(self.emailTxtfield.text)
                ref.child("user").child(uid!).child("password").setValue(self.passwordTxtfield.text)
                let level = Int(self.levelDropDown.text!)
                ref.child("user").child(uid!).child("level").setValue(level)
                ref.child("user").child(uid!).child("blocked").setValue(false)
                ref.child("user").child(uid!).child("deleted").setValue(false)
                ref.child("user").child(uid!).child("firebaseUID").setValue(uid)
                AppGlobals.shared.hideActivityIndicator(view: self.view)
                let alert = UIAlertController(title: "", message: "User has been created", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else {
                AppGlobals.shared.hideActivityIndicator(view: self.view)
                print(error?.localizedDescription)
                let alert = UIAlertController(title: "", message: error?.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    @IBAction func eyeBtnTapped(_ sender: Any) {
        if(iconClick == true) {
            passwordTxtfield.isSecureTextEntry = false
            eyeBtn.setImage(UIImage(named: "eye-visible") , for: .normal)
            
        } else {
            passwordTxtfield.isSecureTextEntry = true
            eyeBtn.setImage(UIImage(named: "eye-invisible") , for: .normal)
        }
        
        iconClick = !iconClick
    }
}
