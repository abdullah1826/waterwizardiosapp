//
//  CalculaionsViewController.swift
//  WaterWizard
//
//  Created by Muhammad Abdullah on 05/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit


class CalculaionsViewController: BaseViewController {

    @IBOutlet weak var firstlbl: UILabel!
    @IBOutlet weak var secondlbl: UILabel!
    @IBOutlet weak var firstSlider: DBNumberedSlider!
    @IBOutlet weak var secondSlider: DBNumberedSlider!
    @IBOutlet weak var displyMessagelbl: UILabel!
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var instruction1lbl: UILabel!{didSet{
        instruction1lbl.isHidden = false
    }}
    @IBOutlet weak var instruction2lbl: UILabel!{didSet{
        instruction2lbl.isHidden = false
    }}
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var displayOR: UILabel! {didSet{
        displayOR.isHidden = true
        }}
    
    let phValuesArray : ((Float,Float),(Float,Float)) = ((6.2,8.4),(7.2,7.8))
    let pharray: [Float] = [ 7.2,7.3,7.4,7.5,7.6,7.7,7.8]
    let ph = 0.001
    let alkalinity = 0.02
    let muriaticAcid = 0.00034
    let stabilizer = 0.00938
    let bromine = 0.006
    let neutralizer = 0.0016
    var volumetoshow : Int!
    
    
    let chlorineValuesArry : ((Float,Float),(Float,Float)) = ((0.0,9.0),(1.0,10.0))
    let chlorine: Float = 0.002
    let alkalanityArray : ((Float,Float),(Float,Float)) = ((0,200),(80,120))
    let BreakpointArray : ((Float,Float),(Float,Float)) = ((0.0,10.0),(1.0,11.0))
    let StabilizerArray : ((Float,Float),(Float,Float)) = ((0,100),(20,50))
    let bromineValuesArry : ((Float,Float),(Float,Float)) = ((0.0,20.0),(5,10))
    var didSelect = ""
    var floatArray = [Int]()
     var floatArray1 = [Float]()
    var chlorinecheck = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        checking volume
        if volumetoshow == 0 {
            let alert = UIAlertController(title: "", message: "Please Select Volume", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        
        }
        
        
        
        
        addBackBtn()
        self.clearImage()

        // didSelect will display row name as a tile
        
        clearAll()
        self.title = didSelect
        switch didSelect {
        case DidselectRow.PH:
            self.title = didSelect
            didSelect = "\(didSelect)"
            let min = phValuesArray.0
            let max = phValuesArray.1
            self.setMinAndMaxValues(min,max,1.0)
        case DidselectRow.CHLORINE:
            self.title = didSelect
            didSelect = "\(didSelect)"
            let min = chlorineValuesArry.0
            let max = chlorineValuesArry.1
             
            self.setMinAndMaxValues(min,max,1.0)
          
        case DidselectRow.ALKALINITY:
            self.title = didSelect
            didSelect = "\(didSelect)"
                       let min = alkalanityArray.0
                       let max = alkalanityArray.1
            firstSlider.labelFormat = "%.0f"
            secondSlider.labelFormat = "%.0f"
                       self.setMinAndMaxValues(min,max,1.0)
           
        case DidselectRow.BREAKPOINT:
            self.title = didSelect
            
            didSelect = "\(didSelect)"
            let min = BreakpointArray.0
                       let max = chlorineValuesArry.1
                        
                       self.setMinAndMaxValues(min,max,1.0)
        case DidselectRow.STABILIZER:
            self.title = didSelect
            didSelect = "\(didSelect)"
                       let min = StabilizerArray.0
                       let max = StabilizerArray.1
            firstSlider.labelFormat = "%.0f"
            secondSlider.labelFormat = "%.0f"
                       self.setMinAndMaxValues(min,max,1.0)
        case DidselectRow.PH:
            self.title = didSelect
            
        case DidselectRow.BROMINE:
            self.title = didSelect
            didSelect = "\(didSelect)"
            let min = bromineValuesArry.0
            let max = bromineValuesArry.1
           
            secondSlider.labelFormat = "%.0f"
            self.setMinAndMaxValues(min,max,1.0)
        default:
            break
        }
        
//        firstlbl.text  = "Current \(didSelect) - min \(phValuesArray[0].0), max \(phValuesArray[0].1)"
//
//        secondlbl.text = "Desired \(didSelect) - min\(phValuesArray[1].0), max \(phValuesArray[1].1)"
        
//        for i in stride(from: phValuesArray[1].0, to: phValuesArray[1].1, by: 0.1) {
//            print(i)
//            floatArray.append(Float(i))
//        }
        
//        firstSlider.minimumValue = Float(phValuesArray[0].0)
//        firstSlider.maximumValue = Float(phValuesArray[0].1)
//        firstSlider.setValue(6.2, animated: false)
//
//        secondSlider.minimumValue = Float(phValuesArray[1].0)
//        secondSlider.maximumValue = Float(phValuesArray[1].1)
//        secondSlider.setValue(7.2, animated: false)
//
//        for i in stride(from: phValuesArray[1].0, through: phValuesArray[1].1, by: 0.1) {
//            print(i)
//            floatArray.append(Float(i))
//        }

       // floatArray.append(Float(phValuesArray[1].1))
        // alternative statement to add something at the end of array instead of append
      //  let _ = floatArray + CollectionOfOne(Float(phValuesArray[1].1))
      
        
                
    }
    
    func setMinAndMaxValues(_ min: (Float,Float), _ max: (Float,Float), _ increment: Float) {
        if didSelect == "PH"{
        firstlbl.text  = "Current \(didSelect) - min \(min.0), max \(min.1)"
        secondlbl.text = "Desired \(didSelect) - min \(max.0), max \(max.1)"
            
        }
        if didSelect == "CHLORINE"{
        firstlbl.text  = "Current \(didSelect) - min \(min.0), max \(min.1)"
        secondlbl.text = "Desired \(didSelect) - min \(max.0), max \(max.1)"
        }
        if didSelect == "ALKALINITY"{
        firstlbl.text  = "Current \(didSelect) - min \(min.0), max \(min.1)"
        secondlbl.text = "Desired \(didSelect) - min \(max.0), max \(max.1)"
        }
        if didSelect == "BREAKPOINT"{
        firstlbl.text  = "Current Free Chlorine \(min.0), max \(min.1)"
        secondlbl.text = "Current Total Chlorine \(min.0), max \(min.1)"
        }
        if didSelect == "STABILIZER"{
               firstlbl.text  = "Current Stabilizer \(min.0), max \(min.1)"
               secondlbl.text = "Desired Stabilizer \(min.0), max \(min.1)"
               }
        if didSelect == "BROMINE"{
                      firstlbl.text  = "Current Bromine \(min.0), max \(min.1)"
                      secondlbl.text = "Desired Bromine \(max.0), max \(max.1)"
                      }
        firstSlider.minimumValue = min.0
        firstSlider.maximumValue = min.1
        firstSlider.setValue(min.0, animated: false)
         
    
        secondSlider.minimumValue = max.0
        secondSlider.maximumValue = max.1
        secondSlider.setValue(max.0, animated: false)
        
        for i in stride(from: max.0, through: max.1, by: increment) {
            print(i)
            floatArray.append(Int(i))
            
        }
        
        
        print(floatArray)
        let range = Float(min.0)
        print("range")
        
               for i in stride(from: max.0, through: max.1, by: increment) {
                   print(i)
                   floatArray1.append(Float(i))
                   
               }
        
        
        
        chlorinecheck == true
        
        if didSelect == DidselectRow.PH {
            let _ = calculateChemicalAmount(convertFloatTo1Decimal(secondSlider.value), 45, Float(ph))
            let _ = displayImage()
            displayMessagePH(range)
        }
        if didSelect == DidselectRow.CHLORINE {
            chlorinecheck = true
           
          
                    let _ = displayImageC()
                    displayMessageC(range)
        }
        if didSelect == DidselectRow.ALKALINITY {
                   chlorinecheck = true
                  
                           
                           displayMessageA(range)
               }
        if didSelect == DidselectRow.BREAKPOINT {
                         chlorinecheck = true
                        
                       
                                 
                                 displayMessageB(range)
                     }
        if didSelect == DidselectRow.STABILIZER {
                         chlorinecheck = true
                        
                       
                                 
                                 displayMessageS(range)
                     }
        if didSelect == DidselectRow.BROMINE {
                                chlorinecheck = true
                               
                              displayMessageBromine(range)
                            }
              
              
        
        
//        firstSlider.labelFormat = String(format: "%.0f", self)

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    @IBAction func moveFirstSlider(_ sender: DBNumberedSlider) {
        firstSlider.value = sender.value
        let range = Float(String(format: "%.1f", sender.value))
        print("range: \(range!)")
       
        if didSelect == "PH"{
        displayMessagePH(range!)
       
        }
        if didSelect == "CHLORINE"{
           displayMessageC(range!)
                 
        }
        if didSelect == "ALKALINITY"{
                 displayMessageA(range!)
                       
              }
        if didSelect == "BREAKPOINT"{
                      displayMessageB(range!)
                            
                   }
        if didSelect == "STABILIZER"{
            displayMessageS(range!)
        }
        
        if didSelect == "BROMINE"{
            displayMessageBromine(range!)
        }
        
    }
    
    @IBAction func moveSecondSlider(_ sender: DBNumberedSlider) {
        secondSlider.value = sender.value
      //  print(String(format: "%.1f", sender.value))
        let range = Float(String(format: "%.1f", sender.value))
//        let currentIndex = floatArray.firstIndex(of: range!)
//        print("Index: \(currentIndex ?? 0)")
        if didSelect == "PH"{
        
             displayMessagePH(range!)
        }
        if didSelect == "CHLORINE"{
            displayMessageC(range!)
        }
        if didSelect == "ALKALINITY"{
            displayMessageA(range!)
        }
        if didSelect == "BREAKPOINT"{
           displayMessageB(range!)
                 
        }
        if didSelect == "STABILIZER"{
                   displayMessageS(range!)
               }
        if didSelect == "BROMINE"{
            displayMessageBromine(range!)
        }
    }
    
    func clearAll(){
        firstlbl.text = ""
        firstSlider.value = 0.0
        secondlbl.text = ""
        secondSlider.value = 0.0
        instruction1lbl.text = ""
        displayOR.isHidden = true
        displyMessagelbl.text = ""
        firstImage.image = nil
        secondImage.image = nil
    }
}


