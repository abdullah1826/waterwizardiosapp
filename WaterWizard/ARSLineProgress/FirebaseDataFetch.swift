//
//  FirebaseDataFetch.swift
//  LifeSign
//
//  Created by Khawar on 4/24/20.
//  Copyright © 2020 Khawar. All rights reserved.
//

import UIKit
import Firebase
import ObjectMapper

let FIREBASE_USERS_TABLE = "users"
let FIREBASE_GAMES_TABLE = "in_progress_games"
let FIREBASE_GAMES_POINT_TABLE = "games_point_table"
let FIREBASE_AUTO_CLICKS_TABLE = "auto_clicks"
let FIREBASE_GAME_NOTIFICATION_COUNT_TABLE = "notification_count"
let FIREBASE_GAME_EXTRA_GAMES_TABLE = "extra_games_table"

class FirebaseDataFetch: NSObject {
    static var shared = FirebaseDataFetch()
    
    var completion: ((String) -> Void)?
    var failure: ((String) -> Void)?
    var dispathGroup = DispatchGroup()

    var databaseRef = Database.database().reference(withPath: "lifesign")
    
    // MARK: - Game
    
    func sendGameRequest(toUserId: Int, completion: @escaping (String)->Void, failure: @escaping (String)->Void) {
        
        self.completion = completion
        self.failure = failure
        
        addGameRequestInSend(toUserId: toUserId)
    }
    
    private func addGameRequestInSend(toUserId:Int){
        
        let loggedUser = User.loggedUser
        
        //Check current logged user game send requests
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let currentLoggedUserRef = userRef.child("\(loggedUser!.id!)")
        
        currentLoggedUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    if let send_requests = userhere?.game_requests_send{
                        //Check if user has already sent game request to this user
                        if send_requests.contains("\(toUserId)") {
                            self.failure!(MCLocalization.string(forKey: already_request_sent)!)
                            return
                        }
                    }
                    
                    if let inProgressGames = userhere?.in_progress_games{
                        //Check already games in progress if user is already in play with this user
                        if inProgressGames.contains("\(toUserId)") {
                            self.failure!(MCLocalization.string(forKey: already_playing_with_user)!)
                            return
                        }
                    }
                    
                    if let rec_requests = userhere?.game_requests_received{
                        //Check already receive game request from this user
                        if rec_requests.contains("\(toUserId)") {
                            self.failure!(MCLocalization.string(forKey: already_receive_game_request)!)
                            return
                        }
                    }
                    
                    //Check if that user is your Game friend, then you don't need to check Extra game available
                    if self.checkIfGameFriend(user: userhere!, opponentUserId: toUserId){
                        //That means user haven't send request to this user
                        self.changeGameFriendStatus(busy:true ,user: userhere!, opponentUserId: toUserId)
                        
                        if let send_requests = userhere?.game_requests_send{
                            //That means current logged user have sent requests, so add user whom he wanna send request again
                            var modifieldRequests = send_requests
                            //That means user haven't send request to this user
                            
                            modifieldRequests.append("\(toUserId)")
                            userhere?.game_requests_send = modifieldRequests
                            (snapshot as AnyObject).ref.setValue(userhere?.toJSON())
                                                        
                            //Now add this game request in the Receive array of requested user
                            self.addGameRequestInReceive(toUserId: toUserId)
                            
                        }else{
                            let sendrequests = ["\(toUserId)"]
                            userhere?.game_requests_send = sendrequests
                            (snapshot as AnyObject).ref.setValue(userhere?.toJSON())
                                                        
                            //Now add this game request in the Receive array of requested user
                            self.addGameRequestInReceive(toUserId: toUserId)
                        }
                    }else{
                        self.canSendGameRequest { (canSend) in
                            if canSend{
                                //That means user haven't send request to this user
                                if let send_requests = userhere?.game_requests_send{
                                    //That means current logged user have sent requests, so add user whom he wanna send request again
                                    var modifieldRequests = send_requests
                                    //That means user haven't send request to this user
                                    
                                    modifieldRequests.append("\(toUserId)")
                                    userhere?.game_requests_send = modifieldRequests
                                    (snapshot as AnyObject).ref.setValue(userhere?.toJSON())
                                    
                                    //Consume Extra game
                                    self.consumeExtraGame()
                                    
                                    //Now add this game request in the Receive array of requested user
                                    self.addGameRequestInReceive(toUserId: toUserId)
                                    
                                }else{
                                    let sendrequests = ["\(toUserId)"]
                                    userhere?.game_requests_send = sendrequests
                                    (snapshot as AnyObject).ref.setValue(userhere?.toJSON())
                                    
                                    //Consume Extra game
                                    self.consumeExtraGame()
                                    
                                    //Now add this game request in the Receive array of requested user
                                    self.addGameRequestInReceive(toUserId: toUserId)
                                }
                            }else{
                                self.failure!(MCLocalization.string(forKey: buy_extra_games)!)
                                return
                            }
                        }
                    }
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: failed_to_send_game_req)!)
            }
            
        })
        
    }
    
    private func addGameRequestInReceive(toUserId:Int){
        
        let loggedUser = User.loggedUser
        let loggedUserId = loggedUser!.id!
        
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let requestReceiverUserRef = userRef.child("\(toUserId)")
        
        requestReceiverUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                                        
                    if let receive_requests = userhere?.game_requests_received{
                        //That means user have receive requests, so add logged user in Receive req
                        print("Receive Requests : \(receive_requests)")
                        var modifieldRequests = receive_requests
                        if !modifieldRequests.contains("\(loggedUserId)") {
                            
                            if let currentGames = userhere?.in_progress_games{
                                //Check already games in progress if user is already in play with this user
                                if currentGames.contains("\(loggedUserId)") {
                                    self.failure!(MCLocalization.string(forKey: already_playing_with_user)!)
                                    return
                                }
                            }
                            
                            //If your game friend then change it's status to busy
                            self.changeGameFriendStatus(busy: true, user: userhere!, opponentUserId: loggedUserId)
                            
                            //Check if that logged user is receiver Game friend, then you don't need to check Extra game available
                            if !self.checkIfGameFriend(user: userhere!, opponentUserId: loggedUserId){
                                //Consume game request receiver free extra game if any
                                self.consumeOpponentFreeExtraGame(forUserId: toUserId)
                            }
                            
                            //That means user haven't receive request from logged user
                            modifieldRequests.append("\(loggedUserId)")
                            userhere?.game_requests_received = modifieldRequests
                            (snapshot as AnyObject).ref.setValue(userhere?.toJSON())
                            self.completion!(MCLocalization.string(forKey: game_req_sent)!)
                            return
                        }else{
                            self.failure!(MCLocalization.string(forKey: already_request_sent)!)
                            return
                        }
                        
                    }else{
                        //That means req receiver user have no previous receive requests, so add logged user in Receive req
                        
                        if let currentGames = userhere?.in_progress_games{
                            //Check already games in progress if user is already in play with this user
                            if currentGames.contains("\(loggedUserId)") {
                                self.failure!(MCLocalization.string(forKey: already_playing_with_user)!)
                                return
                            }
                        }
                        
                        //If your game friend then change it's status to busy
                        self.changeGameFriendStatus(busy: true, user: userhere!, opponentUserId: loggedUserId)
                        
                        //Check if that logged user is receiver Game friend, then you don't need to check Extra game available
                        if !self.checkIfGameFriend(user: userhere!, opponentUserId: loggedUserId){
                            //Consume game request receiver free extra game if any
                            self.consumeOpponentFreeExtraGame(forUserId: toUserId)
                        }

                        let recRequests = ["\(loggedUserId)"]
                        userhere?.game_requests_received = recRequests
                        (snapshot as AnyObject).ref.setValue(userhere?.toJSON())
                        self.completion!(MCLocalization.string(forKey: game_req_sent)!)
                        return
                    }
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: failed_to_send_game_req)!)
                return
            }
            
        })
    }
    
    func checkIfGameFriend(user:User, opponentUserId:Int) -> Bool{
        if let gameFriends = user.game_friends{
            if gameFriends.contains(where: {$0.userId == opponentUserId}){
                return true
            }
        }
        return false
    }
    
    func changeGameFriendStatus(busy:Bool, user:User, opponentUserId:Int){
        if let gameFriends = user.game_friends{
            if gameFriends.contains(where: {$0.userId == opponentUserId}){
                let indexOfFriend = gameFriends.firstIndex(where: {$0.userId == opponentUserId})
                gameFriends[indexOfFriend!].isBusy = busy
                user.game_friends = gameFriends
            }
        }
    }
    
    func canSendGameRequest(canSendCompletion: @escaping (Bool)->Void) {
        
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let loggedUserExtraGamesTableRef = extraGamesRef.child("\(User.loggedUser!.id!)")
        
        loggedUserExtraGamesTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let extragameModel = Mapper<Extra_Games_Model>().map(JSON: snapshotDict as [String : Any])
                    if extragameModel!.unlimited_extra_games!{
                        canSendCompletion(true)
                    }else{
                        if extragameModel!.available_extra_games! > 0 || extragameModel!.free_extra_games! > 0{
                            canSendCompletion(true)
                        }else{
                            canSendCompletion(false)
                        }
                    }
                }
            }else{
                canSendCompletion(false)
            }
        })
    }
    
    func consumeExtraGame() {
        
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let loggedUserExtraGamesTableRef = extraGamesRef.child("\(User.loggedUser!.id!)")
        
        loggedUserExtraGamesTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let extragameModel = Mapper<Extra_Games_Model>().map(JSON: snapshotDict as [String : Any])
                    if !extragameModel!.unlimited_extra_games!{
                        if extragameModel!.free_extra_games! > 0{
                            //If free games available then first consume those
                            extragameModel?.free_extra_games = extragameModel!.free_extra_games! - 1
                            extragameModel?.consume_free_extra_games = extragameModel!.consume_free_extra_games! + 1
                        }else{
                            //Otherwise consume available_extra_games
                            extragameModel?.available_extra_games = extragameModel!.available_extra_games! - 1
                        }
                    }
                    loggedUserExtraGamesTableRef.setValue(extragameModel!.toJSON())
                }
            }
        })
    }
    
    func consumeOpponentFreeExtraGame(forUserId:Int) {
                
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let UserExtraGamesTableRef = extraGamesRef.child("\(forUserId)")
        
        UserExtraGamesTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let extragameModel = Mapper<Extra_Games_Model>().map(JSON: snapshotDict as [String : Any])
                    if !extragameModel!.unlimited_extra_games!{
                        if extragameModel!.free_extra_games! > 0{
                            //If free games available then first consume those
                            extragameModel?.free_extra_games = extragameModel!.free_extra_games! - 1
                            extragameModel?.consume_free_extra_games = extragameModel!.consume_free_extra_games! + 1
                        }
                    }
                    UserExtraGamesTableRef.setValue(extragameModel!.toJSON())
                }
            }
        })
    }
    
    func getAllGameRequests(completion: @escaping (User)->Void, failure: @escaping (String)->Void) {
        
        let loggedUser = User.loggedUser
        let loggedUserId = loggedUser!.id!
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let loggedUserRef = userRef.child("\(loggedUserId)")
        
        loggedUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    completion(userhere!)
                }else{
                    self.failure!(MCLocalization.string(forKey: failed_to_get_game_req)!)
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: failed_to_get_game_req)!)
            }
            
        })
    }
    
    func GameRequests(accept:Bool, opponentUserId:String, completion: @escaping (String)->Void, failure: @escaping (String)->Void) {
        
        //Remove receive request from logged user, and then remove send request from opponent, if accept then add in in progress and create Game object
        self.completion = completion
        self.failure = failure
        
        let loggedUser = User.loggedUser
        let loggedUserId = loggedUser!.id!
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let loggedUserRef = userRef.child("\(loggedUserId)")
        
        loggedUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    if let receive_requests = userhere?.game_requests_received{
                        if receive_requests.contains(opponentUserId) {
                            //Remove from received requests
                            let modifield_receive_requests = receive_requests.filter(){$0 != opponentUserId}
                            userhere?.game_requests_received = modifield_receive_requests
                            
                            if accept{
                                //Add in in progress games
                                if let progress_games = userhere?.in_progress_games{
                                    var modified_progress_games = progress_games
                                    
                                    if !progress_games.contains(opponentUserId) {
                                        modified_progress_games.append(opponentUserId)
                                    }
                                    
                                    userhere?.in_progress_games = modified_progress_games
                                }else{
                                    let progress_games = [opponentUserId]
                                    userhere?.in_progress_games = progress_games
                                }
                                
                                //Decrement in consume free game object,
                                if !self.checkIfGameFriend(user: userhere!, opponentUserId: Int(opponentUserId)!){
                                    self.decrementConsumeFreeExtraGame(userId: "\(loggedUserId)", decreCompletion: { (resp) in
                                        loggedUserRef.setValue(userhere?.toJSON())
                                    })
                                }else{
                                    loggedUserRef.setValue(userhere?.toJSON())
                                }
                            }else{
                                //In reject case, add Extra game in sender extra games table, bcz game was not started
                                if self.checkIfGameFriend(user: userhere!, opponentUserId: Int(opponentUserId)!){
                                    self.changeGameFriendStatus(busy: false, user: userhere!, opponentUserId: Int(opponentUserId)!)
                                    loggedUserRef.setValue(userhere?.toJSON())
                                }else{
                                    self.incrementExtraGames(userId: opponentUserId, increCompletion: { (resp) in
                                        loggedUserRef.setValue(userhere?.toJSON())
                                    }) //In opponent extra game
                                }
                            }
                            
                            //Now remove from send request from opponent User
                            self.removeGameReqFromOpponent(opponentUserId, accept: accept)
                        }
                    }
                }else{
                    if accept{
                        self.failure!(MCLocalization.string(forKey: failed_to_accept_game_req)!)
                    }else{
                        self.failure!(MCLocalization.string(forKey: failed_to_reject_game_req)!)
                    }
                }
                
            }else{
                if accept{
                    self.failure!(MCLocalization.string(forKey: failed_to_accept_game_req)!)
                }else{
                    self.failure!(MCLocalization.string(forKey: failed_to_reject_game_req)!)
                }
            }
            
        })
    }
    
    private func removeGameReqFromOpponent(_ opponentUserId:String, accept:Bool) {
        
        let loggedUser = User.loggedUser
        let loggedUserId = "\(loggedUser!.id!)"
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let opponentUserRef = userRef.child(opponentUserId)
        
        opponentUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    if let send_requests = userhere?.game_requests_send{
                        if send_requests.contains(loggedUserId) {
                            let modifield_send_requests = send_requests.filter(){$0 != loggedUserId}
                            userhere?.game_requests_send = modifield_send_requests
                            
                            if accept{
                                //Add in in progress games
                                if let progress_games = userhere?.in_progress_games{
                                    var modified_progress_games = progress_games
                                    
                                    if !progress_games.contains(loggedUserId) {
                                        modified_progress_games.append(loggedUserId)
                                    }
                                    
                                    userhere?.in_progress_games = modified_progress_games
                                }else{
                                    let progress_games = [loggedUserId]
                                    userhere?.in_progress_games = progress_games
                                }
                                
                                //Decrement in consume free game object,
                                if !self.checkIfGameFriend(user: userhere!, opponentUserId: loggedUser!.id!){
                                    self.decrementConsumeFreeExtraGame(userId: opponentUserId, decreCompletion: { (resp) in
                                        opponentUserRef.setValue(userhere?.toJSON())
                                    })
                                }else{
                                    opponentUserRef.setValue(userhere?.toJSON())
                                }
                                
                                //Create new running game
                                self.addRunningGame(opponentUserId)
                            }else{
                                //In reject case, add Extra game in sender extra games table, bcz game was not started
                                self.changeGameFriendStatus(busy: false, user: userhere!, opponentUserId: loggedUser!.id!)
                                
                                //Increment in consume free game object,
                                //Increment in my extra game, am rejecting this
                                if !self.checkIfGameFriend(user: userhere!, opponentUserId: loggedUser!.id!){
                                    self.incrementExtraGames(userId: loggedUserId, increCompletion: { (resp) in
                                        opponentUserRef.setValue(userhere?.toJSON())
                                    })
                                }else{
                                    opponentUserRef.setValue(userhere?.toJSON())
                                }

                            }
                        }
                    }
                }else{
                    self.failure!(MCLocalization.string(forKey: failed_to_reject_game_req)!)
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: failed_to_reject_game_req)!)
            }
            
        })
    }
    
    private func addRunningGame(_ opponentUserId:String){
        
        //Add new game
        let loggedUser = User.loggedUser
        let loggedUserId = "\(loggedUser!.id!)"
        
        let gamesRef = self.databaseRef.child(FIREBASE_GAMES_TABLE)
        
        var gameId = ""
        if loggedUser!.id! < Int(opponentUserId)!{
            gameId = "\(loggedUserId)_\(opponentUserId)"
        }else{
            gameId = "\(opponentUserId)_\(loggedUserId)"
        }
        
        let newGameRef = gamesRef.child(gameId)
        
        let futureTime = Calendar.current.date(byAdding: .hour, value: GAME_PLAY_HOUR_LIMIT, to: Date())!
        let gamePlayHoursInseconds = GAME_PLAY_HOUR_LIMIT * 3600
        
        let gameData = ["started_by":Int(opponentUserId)!, "other_user":loggedUser!.id!, "start_time": Date().getUTCDateString(), "reply_time_hours":futureTime.getUTCDateString(), "time_left":gamePlayHoursInseconds, "started_by_replied": false, "other_user_replied": false, "hits":0] as [String : Any]
        newGameRef.setValue(gameData)
        
        self.completion!(MCLocalization.string(forKey: game_added_successfully)!)
        
    }
    
    func fetchRunningGameData(_ opponentUserId:String, completion: @escaping (Game_In_Progress)->Void, failure: @escaping (String)->Void){
        
        //Add new game
        let loggedUser = User.loggedUser
        let loggedUserId = "\(loggedUser!.id!)"
        
        let gamesRef = self.databaseRef.child(FIREBASE_GAMES_TABLE)
        
        var gameId = ""
        if loggedUser!.id! < Int(opponentUserId)!{
            gameId = "\(loggedUserId)_\(opponentUserId)"
        }else{
            gameId = "\(opponentUserId)_\(loggedUserId)"
        }
        
        let runningGameRef = gamesRef.child(gameId)
        
        runningGameRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let game = Mapper<Game_In_Progress>().map(JSON: snapshotDict as [String : Any])
                    completion(game!)
                }else{
                    self.failure!(MCLocalization.string(forKey: failed_to_get_game_data)!)
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: failed_to_get_game_data)!)
            }
            
        })
    }
    
    func clickGame(_ opponentUserId:String, completion: @escaping (String)->Void, failure: @escaping (String)->Void){
        self.fetchRunningGameData(opponentUserId, completion: { (game) in
            
            let gamesRef = self.databaseRef.child(FIREBASE_GAMES_TABLE)
            
            var gameId = ""
            
            if game.other_user! < game.started_by!{
                gameId = "\(game.other_user!)_\(game.started_by!)"
            }else{
                gameId = "\(game.started_by!)_\(game.other_user!)"
            }
            
            let runningGameRef = gamesRef.child(gameId)
            
            if game.started_by! == User.loggedUser!.id!{
                //Started by is You
                game.started_by_replied = true
                game.other_user_replied = false
            }else{
                //Other user is You
                game.started_by_replied = false
                game.other_user_replied = true
            }
            
            game.time_left = self.getTimeLeftString(time_left: game.time_left!)
            game.hits = game.hits! + 1
            
            //Reply time should be according to time_left
            
            let futureTime = Calendar.current.date(byAdding: .second, value: game.time_left!, to: Date())!
            game.reply_time_hours = futureTime.getUTCDateString()
            
            runningGameRef.setValue(game.toJSON())
            self.sendNotification(toUserId: Int(opponentUserId)!, text: MCLocalization.string(forKey: played_it_turn)!)
            completion("success")
        }) { (message) in
            failure("")
        }
    }
    
    private func getTimeLeftString(time_left:Int) -> Int{
        
        let timeInHours = time_left/3600 //Convert seconds to Horurs
        
        switch timeInHours {
        case let val where val > 2: //Greater than 2 hours
            let hoursMinu2 = timeInHours - 2
            return hoursMinu2 * 3600 //Convert hours to seconds
        case let val where val == 2: //Equal to 2 hours
            let timeInMinutes = time_left/60 //Convert seconds to Minutes
            let minutesMinus5 = timeInMinutes - 5 //Minus 5 minutes
            return minutesMinus5 * 60 //Convert minutes to seconds
        default: //Less than 2 hours
            let timeInMinutes = time_left/60 //Convert seconds to Minutes
            if timeInMinutes <= 5{
                return 5 * 60 //Convert 5 minutes to seconds
            }else{
                let minutesMinus5 = timeInMinutes - 5
                return minutesMinus5 * 60 //Convert minutes to seconds
            }
        }
    }
    
    func endGameAddScores(_ opponentUserId:String, endGameCompletion: @escaping (String)->Void){
        
        //1 point for initiative
        //10 For Winner
        //3 For looser
        
        self.fetchRunningGameData(opponentUserId, completion: { (game) in
            
            let gamesRef = self.databaseRef.child(FIREBASE_GAMES_TABLE)
            
            var gameId = ""
            
            if game.other_user! < game.started_by!{
                gameId = "\(game.other_user!)_\(game.started_by!)"
            }else{
                gameId = "\(game.started_by!)_\(game.other_user!)"
            }
            
            let runningGameRef = gamesRef.child(gameId)
            
            var myPoints = 0
            var opponentPoints = 0
            
            if game.started_by! == User.loggedUser!.id!{
                //Started by is You
                
                myPoints += 1 //For game initaitive
                
                if game.started_by_replied!{
                    //You Won
                    myPoints += 10
                    opponentPoints += 3
                    game.Won_by = game.started_by!
                }else{
                    //You Loose
                    myPoints += 3
                    opponentPoints += 10
                    game.Won_by = game.other_user!
                }
                
            }else{
                //Other user is You
                opponentPoints += 1 //For game initaitive
                
                if game.other_user_replied!{
                    //You Won
                    myPoints += 10
                    opponentPoints += 3
                    game.Won_by = game.other_user!
                }else{
                    //You Loose
                    myPoints += 3
                    opponentPoints += 10
                    game.Won_by = game.started_by!
                }
            }
            
            print("My ID : \(User.loggedUser!.id!), My Points: \(myPoints)")
            print("Opponent ID : \(opponentUserId), Opponent Points: \(opponentPoints)")
            print("Game won by ID: \(game.Won_by!)")
            
            runningGameRef.setValue(game.toJSON())
            self.addGamePointsInDatabase(opponentUserId, myPoints: myPoints, opponentPoints: opponentPoints)
            endGameCompletion("Done")
            //Add all points in Games Table
        }) { (message) in
            endGameCompletion("Fail")
        }
    }
    
    private func addGamePointsInDatabase(_ opponentUserId:String, myPoints:Int, opponentPoints:Int){
        let gamesRef = self.databaseRef.child(FIREBASE_GAMES_POINT_TABLE)
        
        //Add points in your table
        let myGamePointsTableRef = gamesRef.child("\(User.loggedUser!.id!)")
        let opponentPointsTableRef = gamesRef.child(opponentUserId)
        
        self.updatePointsTable(tableRef: myGamePointsTableRef, points: myPoints)
        self.updatePointsTable(tableRef: opponentPointsTableRef, points: opponentPoints)
        
    }
    
    private func updatePointsTable(tableRef:DatabaseReference, points:Int){
        tableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let previouspoints = snapshot?.value as? Int {
                    let newPoints = previouspoints + points
                    tableRef.setValue(newPoints)
                }else{
//                    self.failure!("")
                }
                
            }else{
                //No record of this user exist
                tableRef.setValue(points)
            }
            
        })
    }
    
    func removeGameFromInProgress(_ opponentUserId:String){
        
        
        let loggedUser = User.loggedUser
        let loggedUserId = loggedUser!.id!
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let loggedUserRef = userRef.child("\(loggedUserId)")
        
        loggedUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    if let progress_games = userhere?.in_progress_games{
                        var modified_progress_games = progress_games
                        
                        //Remove from in progress
                        if progress_games.contains(opponentUserId) {
                            modified_progress_games = modified_progress_games.filter(){$0 != opponentUserId}
                        }
                        
                        userhere?.in_progress_games = modified_progress_games
                        
                        //Add/Update this user as your Game Friend
                        let newFriendDict = ["userId":Int(opponentUserId)!, "isBusy":false] as [String : Any]
                        let newFriendMapper = Mapper<GameFriend>().map(JSON: newFriendDict)

                        if let gameFriends = userhere?.game_friends{
                            var modified_gameFriends = gameFriends
                            
                            //Check if this friend is already your game friend or not
                            if gameFriends.contains(where: {$0.userId == Int(opponentUserId)!}){
                                //Update game friend to available for next game
                                let frndIndex = gameFriends.firstIndex(where: {$0.userId == Int(opponentUserId)!})
                                modified_gameFriends[frndIndex!] = newFriendMapper!
                            }else{
                                modified_gameFriends.append(newFriendMapper!)
                            }
                            userhere?.game_friends = modified_gameFriends

                        }else{
                            userhere?.game_friends = [newFriendMapper!]
                        }
                    }
                    
                    loggedUserRef.setValue(userhere?.toJSON())
                    self.removeGameFromInProgressOpponentSide(opponentUserId)
                    
                }else{
                    self.failure!(MCLocalization.string(forKey: failed_to_update_game_data)!)
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: failed_to_update_game_data)!)
            }
            
        })
    }
    
    private func removeGameFromInProgressOpponentSide(_ opponentUserId:String){
        
        
        let loggedUser = User.loggedUser
        let loggedUserId = "\(loggedUser!.id!)"
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let opponentUserRef = userRef.child(opponentUserId)
        
        opponentUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    if let progress_games = userhere?.in_progress_games{
                        var modified_progress_games = progress_games
                        
                        //Remove from in progress
                        if progress_games.contains(loggedUserId) {
                            modified_progress_games = modified_progress_games.filter(){$0 != loggedUserId}
                        }
                        
                        userhere?.in_progress_games = modified_progress_games
                        
                        //Add/Update this user as your Game Friend
                         let newFriendDict = ["userId":loggedUser!.id!, "isBusy":false] as [String : Any]
                         let newFriendMapper = Mapper<GameFriend>().map(JSON: newFriendDict)

                         if let gameFriends = userhere?.game_friends{
                             var modified_gameFriends = gameFriends
                             
                             //Check if this friend is already your game friend or not
                             if gameFriends.contains(where: {$0.userId == loggedUser!.id!}){
                                 //Update game friend to available for next game
                                 let frndIndex = gameFriends.firstIndex(where: {$0.userId == loggedUser!.id!})
                                 modified_gameFriends[frndIndex!] = newFriendMapper!
                             }else{
                                 modified_gameFriends.append(newFriendMapper!)
                             }
                             userhere?.game_friends = modified_gameFriends

                         }else{
                             userhere?.game_friends = [newFriendMapper!]
                         }

                    }
                    
                    opponentUserRef.setValue(userhere?.toJSON())
                    self.removeGameFromInProgressTable(opponentUserId)
                    
                }else{
                    self.failure!(MCLocalization.string(forKey: failed_to_update_game_data)!)
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: failed_to_update_game_data)!)
            }
            
        })
    }
    
    private func removeGameFromInProgressTable(_ opponentUserId:String){
        
        let loggedUser = User.loggedUser
        let loggedUserId = loggedUser!.id!
        
        var gameId = ""
        
        if Int(opponentUserId)! < loggedUserId{
            gameId = "\(opponentUserId)_\(loggedUserId)"
        }else{
            gameId = "\(loggedUserId)_\(opponentUserId)"
        }
        
        let gamesRef = self.databaseRef.child(FIREBASE_GAMES_TABLE)
        let runningGameRef = gamesRef.child(gameId)
        
        runningGameRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                snapshot?.ref.removeValue()
                
            }else{
                self.failure!(MCLocalization.string(forKey: failed_to_update_game_data)!)
            }
            
        })
    }
    
    func getGameScores(completion: @escaping ([scores])->Void, failure: @escaping (String)->Void) {
        
        let loggedUser = User.loggedUser
        let _ = loggedUser!.id!
        
        let scoreRef = self.databaseRef.child(FIREBASE_GAMES_POINT_TABLE)
        
        scoreRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            var plyaerScores = [scores]()
            
            if (snapshot?.exists())! {
                for item in (snapshot?.children)! {
                    if  let childSnapshot = item as? DataSnapshot {
                        guard let score = childSnapshot.value else {
                            completion(plyaerScores)
                            return
                        }
                        
                        let userId = childSnapshot.key
                        plyaerScores.append(scores(userId: Int(userId)!, score: score as! Int, user: nil))
                    }
                }
                completion(plyaerScores)
            }else {
                failure("No data found")
            }
        })
    }
    
    func updateAutoClicks(autoClicks:Int){
        
        let autoClicksRef = self.databaseRef.child(FIREBASE_AUTO_CLICKS_TABLE)
        let userAutoClicksRef = autoClicksRef.child("\(User.loggedUser!.id!)")
        
        userAutoClicksRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            userAutoClicksRef.setValue(autoClicks)
            
        })
    }
    
    func getAutoClicks(completion: @escaping (Int)->Void){
        
        let autoClicksRef = self.databaseRef.child(FIREBASE_AUTO_CLICKS_TABLE)
        let userAutoClicksRef = autoClicksRef.child("\(User.loggedUser!.id!)")
        
        userAutoClicksRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                guard let clicks = snapshot?.value else {
                    completion(0)
                    return
                }
                completion(clicks as! Int)
            }else{
                completion(0)
            }
        })
    }
    
    
    func updateGameNotificationCount(forUserId:Int, count:Int, allRead:Bool, completion: @escaping (Int)->Void){
        
        let gameCountRef = self.databaseRef.child(FIREBASE_GAME_NOTIFICATION_COUNT_TABLE)
        let userNotiCountRef = gameCountRef.child("\(forUserId)")
        
        userNotiCountRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                guard let prevCount = snapshot?.value else {
                    userNotiCountRef.setValue(count)
                    completion(0)
                    return
                }
                if allRead{
                    userNotiCountRef.setValue(0)
                }else{
                    var previousCount = prevCount as! Int
                    previousCount += count
                    userNotiCountRef.setValue(previousCount)
                }
                completion(0)
            }else{
                userNotiCountRef.setValue(count)
                completion(0)
            }
        })
    }
    
    func getGameNotificationCount(completion: @escaping (Int)->Void){
        
        let gameCountRef = self.databaseRef.child(FIREBASE_GAME_NOTIFICATION_COUNT_TABLE)
        let userNotiCountRef = gameCountRef.child("\(User.loggedUser!.id!)")
        
        userNotiCountRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            if (snapshot?.exists())! {
                guard let prevCount = snapshot?.value else {
                    completion(0)
                    return
                }
                completion(prevCount as! Int)
            }else{
                completion(0)
            }
        })
    }
    
    func removeGameFriend(_ opponentUserId:String, completion: @escaping (String)->Void, failure: @escaping (String)->Void){
        
        self.completion = completion
        self.failure = failure

        let loggedUser = User.loggedUser
        let loggedUserId = "\(loggedUser!.id!)"
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let loggedUserRef = userRef.child(loggedUserId)
        
        loggedUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    if let gameFriends = userhere?.game_friends{
                        var modified_gameFriends = gameFriends
                        
                        //Check if this friend exist, if exist remove that friend
                        if gameFriends.contains(where: {$0.userId == Int(opponentUserId)!}){
                            //Update game friend to available for next game
                            modified_gameFriends = modified_gameFriends.filter(){$0.userId != Int(opponentUserId)!}
                        }
                        
                        userhere?.game_friends = modified_gameFriends
                    }
                    
                    loggedUserRef.setValue(userhere?.toJSON())
                    self.removeGameFriendFromOpponentSide(opponentUserId)
                    
                }else{
                    self.failure!(MCLocalization.string(forKey: remove_game_friend_failed)!)
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: remove_game_friend_failed)!)
            }
            
        })
    }
    
    private func removeGameFriendFromOpponentSide(_ opponentUserId:String){
        
        let loggedUser = User.loggedUser
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let opponentUserRef = userRef.child(opponentUserId)
        
        opponentUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    if let gameFriends = userhere?.game_friends{
                        var modified_gameFriends = gameFriends
                        
                        //Check if this friend exist, if exist remove that friend
                        if gameFriends.contains(where: {$0.userId == loggedUser!.id!}){
                            //Update game friend to available for next game
                            modified_gameFriends = modified_gameFriends.filter(){$0.userId != loggedUser!.id!}
                        }
                        
                        userhere?.game_friends = modified_gameFriends
                    }
                    
                    opponentUserRef.setValue(userhere?.toJSON())
                    self.completion!(MCLocalization.string(forKey: remove_game_friend_success)!)
                }else{
                    self.failure!(MCLocalization.string(forKey: remove_game_friend_failed)!)
                }
                
            }else{
                self.failure!(MCLocalization.string(forKey: remove_game_friend_failed)!)
            }
            
        })
    }
    // MARK: - User
    
    func getUser(_ userId:Int, completion: @escaping (User)->Void, failure: @escaping (String)->Void) {
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let userRefToFetch = userRef.child("\(userId)")
        
        userRefToFetch.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    completion(userhere!)
                }else{
                    self.failure!("")
                }
                
            }else{
                self.failure!("")
            }
            
        })
    }
    
    func updateUser(_ user:User) {
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let userRefToFetch = userRef.child("\(user.id!)")
        
        userRefToFetch.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    user.game_requests_send = userhere?.game_requests_send
                    user.game_requests_received = userhere?.game_requests_received
                    user.in_progress_games = userhere?.in_progress_games
                    user.game_friends = userhere?.game_friends

                    userRefToFetch.setValue(user.toJSON())
                    self.checkForExtraGamesTable(user: user)
                }
            }else{
                userRefToFetch.setValue(user.toJSON())
            }
        })
    }
    
    func removeFCM() {
        
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let userRefToFetch = userRef.child("\(User.loggedUser!.id!)")
        
        userRefToFetch.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    userhere!.fcm_token = ""
                    userRefToFetch.setValue(userhere!.toJSON())
                }
            }
        })
    }
    
    func signIn(withEmail:String, password:String,user:User){
        Auth.auth().signIn(withEmail: withEmail, password: password) {
            authResult, error in
            
            if error == nil{
                self.updateUser(user)
            }else{
                //Sign up User on firebase
                self.signUp(withEmail: withEmail, password: password, user: user)
            }
        }
    }
    
    func signUp(withEmail:String, password:String,user:User){
        //Sign up User on firebase
        Auth.auth().createUser(withEmail: withEmail, password: password) { authResult, error in
            
            let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
            
            let currentUserRef = userRef.child("\(user.id!)")
            currentUserRef.setValue(user.toJSON())
            self.addDefaultExtraGames(user: user)

            if error != nil{
                self.signIn(withEmail: withEmail, password: password, user: user)
            }
        }
    }
    
    func checkForExtraGamesTable(user:User){
        
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let currentUserRef = extraGamesRef.child("\(user.id!)")

        currentUserRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if !(snapshot?.exists())! {
                self.addDefaultExtraGames(user: user)
            }
        })
    }
    
    func addDefaultExtraGames(user:User){
        //Sign up User on firebase
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        
        let currentUserRef = extraGamesRef.child("\(user.id!)")
        
        let extraGameDict = ["available_extra_games":0, "free_extra_games":3, "consume_free_extra_games":0, "unlimited_extra_games":false] as [String : Any]
        currentUserRef.setValue(extraGameDict)
    }
    
    func updateExtraGames(unlimitedExtraGames:Bool,extraGames:Int) {
        
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let loggedUserExtraGamesTableRef = extraGamesRef.child("\(User.loggedUser!.id!)")
        
        loggedUserExtraGamesTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let extragameModel = Mapper<Extra_Games_Model>().map(JSON: snapshotDict as [String : Any])
                    extragameModel?.unlimited_extra_games = unlimitedExtraGames
                    if unlimitedExtraGames{
                        extragameModel?.available_extra_games = MAX_GAME_LIMIT
                    }else{
                        extragameModel?.available_extra_games = extraGames
                    }
                    loggedUserExtraGamesTableRef.setValue(extragameModel!.toJSON())
                }
            }
        })
    }
    
    func incrementExtraGames(userId:String, increCompletion: @escaping (String)->Void) {
        
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let UserExtraGamesTableRef = extraGamesRef.child(userId)
        
        UserExtraGamesTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let extragameModel = Mapper<Extra_Games_Model>().map(JSON: snapshotDict as [String : Any])
                    if !extragameModel!.unlimited_extra_games!{
                        if extragameModel!.consume_free_extra_games! > 0{
                            extragameModel?.consume_free_extra_games = extragameModel!.consume_free_extra_games! - 1
                            extragameModel?.free_extra_games = extragameModel!.free_extra_games! + 1
                        }else{
                            extragameModel?.available_extra_games = extragameModel!.available_extra_games! + 1
                        }
                    }
                    UserExtraGamesTableRef.setValue(extragameModel!.toJSON())
                    increCompletion("Done")
                }
            }
        })
    }
    
    func decrementConsumeFreeExtraGame(userId:String, decreCompletion: @escaping (String)->Void) {
        
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let UserExtraGamesTableRef = extraGamesRef.child(userId)
        
        UserExtraGamesTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let extragameModel = Mapper<Extra_Games_Model>().map(JSON: snapshotDict as [String : Any])
                    if extragameModel!.consume_free_extra_games! > 0{
                        extragameModel?.consume_free_extra_games = extragameModel!.consume_free_extra_games! - 1
                    }
                    UserExtraGamesTableRef.setValue(extragameModel!.toJSON())
                    decreCompletion("Done")
                }
            }
        })
    }
    
    func getAvailableExtraGames(completion: @escaping (Extra_Games_Model)->Void) {
        
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let loggedUserExtraGamesTableRef = extraGamesRef.child("\(User.loggedUser!.id!)")
        
        loggedUserExtraGamesTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let extragameModel = Mapper<Extra_Games_Model>().map(JSON: snapshotDict as [String : Any])
                    completion(extragameModel!)
                }else{
                    let extraGameDict = ["available_extra_games":0, "free_extra_games":0, "consume_free_extra_games":0, "unlimited_extra_games":false] as [String : Any]
                    let extragameModel = Mapper<Extra_Games_Model>().map(JSON: extraGameDict)
                    completion(extragameModel!)
                }
                
            }else{
                let extraGameDict = ["available_extra_games":0, "free_extra_games":0, "consume_free_extra_games":0, "unlimited_extra_games":false] as [String : Any]
                let extragameModel = Mapper<Extra_Games_Model>().map(JSON: extraGameDict)
                completion(extragameModel!)
            }
        })
    }
    
    func showError(error: String, viewController: UIViewController) {
        
    }
    
    func sendNotification(toUserId:Int, text:String){
        
        self.getUser(toUserId, completion: { (fetchedUser) in
            let url = NSURL(string: "https://fcm.googleapis.com/fcm/send")
            let gameImg = #imageLiteral(resourceName: "AddGame")
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            var imgFilePath = ""
            // Save image.
            if let filePath = paths.first?.appendingPathComponent("gameICon.png") {
                // Save image.
                do {
                    try gameImg.pngData()?.write(to: filePath, options: .atomic)
                    imgFilePath = filePath.absoluteString
                } catch {
                    // Handle the error
                }
            }
            
            let postParams: [String : AnyObject] = ["apns": [
                "payload": [
                    "aps": [
                        "mutable-content": 1
                    ]
                ],
                "fcm_options": [
                    "image": "http://uztrip.twaintec.com/images/users/Khawar-5ecf6c05f1c82.jpg"
                ]
                ] as AnyObject, "to":fetchedUser.fcm_token! as AnyObject,"data": ["type":"Game","name":"","user_id":""] as AnyObject, "notification": ["body": "\(User.loggedUser!.first_name!) \(text)", "title": "\(User.loggedUser!.first_name!)" ,"sound" : "default"] as AnyObject]
            //, "badge" : totalBadgeCount
            let request = NSMutableURLRequest(url: url! as URL)
            request.httpMethod = "POST"
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.setValue("key=AAAA66GYH1o:APA91bGOJFfugtsDwq4FKy4stuhUu6F3cJK9fZQZ7THnS_FaphusnZ0najvHzRYIQhfP2ONto6UeEuQIEdxpZrNSF86Kp6plahqWzkalyEHyuBzbEvUqg9rpRs3bsL3X4UUCiuW_R_xg", forHTTPHeaderField: "Authorization")
            
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: postParams, options: JSONSerialization.WritingOptions())
                print("My paramaters: \(postParams)")
            }
            catch{
                print("Caught an error: \(error)")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if let realResponse = response as? HTTPURLResponse{
                    if realResponse.statusCode != 200{
                        print("Not a 200 response")
                    }
                }
                
                if let postString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as String?{
                    print("POST: \(postString)")
                }
            }
            
            task.resume()
        }) { (mess) in}
    }
    
    func deleteUserAndRemoveAllData(DeleteCompletion: @escaping (String)->Void, DeleteFailure: @escaping (String)->Void){
        
        dispathGroup.enter()
        self.removeDeletedUserAutoClicks()
        
        dispathGroup.enter()
        self.removeDeletedUserExtraGames()
        
        dispathGroup.enter()
        self.removeDeletedUserGamePoints()
        
        dispathGroup.enter()
        self.removeDeletedUserNotifications()
        
        dispathGroup.enter()
        self.removeDeletedUserObject()
        
        dispathGroup.enter()
        self.removeDeletedUserRunningGames()
        
        dispathGroup.enter()
        self.removeDeletedUserFriendsFromOtherUsers()
        
        dispathGroup.notify(queue: .main) {
            let user = Auth.auth().currentUser
            user?.delete { error in
              if let error = error {
                // An error happened.
                DeleteCompletion(error.localizedDescription)
              } else {
                // Account deleted, now delete app corresponding data
                DeleteCompletion("Done")
              }
            }
        }
        
    }
    
    func removeDeletedUserAutoClicks(){
        let autoClicksRef = self.databaseRef.child(FIREBASE_AUTO_CLICKS_TABLE)
        let userAutoClicksRef = autoClicksRef.child("\(User.loggedUser!.id!)")
        
        userAutoClicksRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                userAutoClicksRef.removeValue()
            }
            self.dispathGroup.leave()
        })
    }
    
    func removeDeletedUserExtraGames(){
        let extraGamesRef = self.databaseRef.child(FIREBASE_GAME_EXTRA_GAMES_TABLE)
        let loggedUserExtraGamesTableRef = extraGamesRef.child("\(User.loggedUser!.id!)")
        
        loggedUserExtraGamesTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                loggedUserExtraGamesTableRef.removeValue()
            }
            self.dispathGroup.leave()
        })
    }
    
    func removeDeletedUserGamePoints(){
        let pointsTblRef = self.databaseRef.child(FIREBASE_GAMES_POINT_TABLE)
        let loggedUserPointsTableRef = pointsTblRef.child("\(User.loggedUser!.id!)")
        
        loggedUserPointsTableRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                loggedUserPointsTableRef.removeValue()
            }
            self.dispathGroup.leave()
        })
    }
    
    func removeDeletedUserNotifications(){
        
        let gameCountRef = self.databaseRef.child(FIREBASE_GAME_NOTIFICATION_COUNT_TABLE)
        let userNotiCountRef = gameCountRef.child("\(User.loggedUser!.id!)")
        
        userNotiCountRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            if (snapshot?.exists())! {
                userNotiCountRef.removeValue()
             }
            self.dispathGroup.leave()
        })
    }
    
    func removeDeletedUserObject() {
        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let userRefToFetch = userRef.child("\(User.loggedUser!.id!)")
        
        userRefToFetch.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                userRefToFetch.removeValue()
            }
            self.dispathGroup.leave()
        })
    }
    
    func removeDeletedUserRunningGames(){
        
        //Add new game
        let loggedUser = User.loggedUser
        let loggedUserId = "\(loggedUser!.id!)"
        
        let gamesRef = self.databaseRef.child(FIREBASE_GAMES_TABLE)
        
        gamesRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                for item in (snapshot?.children)! {
                    if  let childSnapshot = item as? DataSnapshot {
                        let runningGameId = childSnapshot.key
                        if runningGameId.components(separatedBy: "_").contains(loggedUserId){
                            //Remove this game
                            (item as AnyObject).ref?.removeValue()
                        }
                    }
                }
            }
            self.dispathGroup.leave()
        })
    }
    
    func removeDeletedUserFriendsFromOtherUsers(){
        
        //Add new game
        let loggedUser = User.loggedUser
        
        let usersRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        
        usersRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                for item in (snapshot?.children)! {
                    if  let childSnapshot = item as? DataSnapshot {
                        if let snapshotDict = childSnapshot.value as? Dictionary<String, Any?> {
                            let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                            if let gameFriends = userhere?.game_friends{
                                var modified_gameFriends = gameFriends
                                
                                //Check if this friend exist, if exist remove that friend
                                if gameFriends.contains(where: {$0.userId == loggedUser!.id!}){
                                    //Update game friend to available for next game
                                    modified_gameFriends = modified_gameFriends.filter(){$0.userId != loggedUser!.id!}
                                    userhere?.game_friends = modified_gameFriends
                                    (item as AnyObject).ref?.setValue(userhere?.toJSON())
                                }
                            }
                            
                            //Remove In progress Games
                            if let inProgressGames = userhere?.in_progress_games{
                                var modified_inProgressGames = inProgressGames
                                
                                //Check if this friend exist, if exist remove that friend
                                if modified_inProgressGames.contains("\(loggedUser!.id!)"){
                                    //Update game friend to available for next game
                                    modified_inProgressGames = modified_inProgressGames.filter(){ !$0.contains("\(loggedUser!.id!)") }
                                    userhere?.in_progress_games = modified_inProgressGames
                                    (item as AnyObject).ref?.setValue(userhere?.toJSON())
                                }
                            }
                        }
                    }
                }
            }
            self.dispathGroup.leave()
        })
    }
    
    func removeBlockUserData(friendId:Int){
        self.removeRunningGamesWithBlockUser(friendId:friendId)
    }
    
    func removeRunningGamesWithBlockUser(friendId:Int){
        
        //Add new game
        let loggedUser = User.loggedUser
        let loggedUserId = "\(loggedUser!.id!)"
        
        let gamesRef = self.databaseRef.child(FIREBASE_GAMES_TABLE)
        
        gamesRef.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                for item in (snapshot?.children)! {
                    if  let childSnapshot = item as? DataSnapshot {
                        let runningGameId = childSnapshot.key
                        let combination0 = "\(loggedUserId)_\(friendId)"
                        let combination1 = "\(friendId)_\(loggedUserId)"
                        if runningGameId == combination0 || runningGameId == combination1{
                            //Remove this game
                            (item as AnyObject).ref?.removeValue()
                        }
                    }
                }
            }
        })
    }

    func removeBlockFriendFromGameFriend(friendId:Int){
        let loggedUser = User.loggedUser

        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let userRefToFetch = userRef.child("\(loggedUser!.id!)")
        
        userRefToFetch.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    
                    //Remove From Game Friends
                    if let gameFriends = userhere?.game_friends{
                        var modified_gameFriends = gameFriends
                        
                        //Check if this friend exist, if exist remove that friend
                        if gameFriends.contains(where: {$0.userId == friendId}){
                            //Update game friend to available for next game
                            modified_gameFriends = modified_gameFriends.filter(){$0.userId != friendId}
                            userhere?.game_friends = modified_gameFriends
                        }
                    }
                    
                    //Remove In progress Games
                    if let inProgressGames = userhere?.in_progress_games{
                        var modified_inProgressGames = inProgressGames
                        
                        //Check if this friend exist, if exist remove that friend
                        if modified_inProgressGames.contains("\(friendId)"){
                            //Update game friend to available for next game
                            modified_inProgressGames = modified_inProgressGames.filter(){ !$0.contains("\(friendId)") }
                            userhere?.in_progress_games = modified_inProgressGames
                        }
                    }
                    
                    userRefToFetch.setValue(userhere?.toJSON())
                }
            }
        })
    }
    
    func updateFCMToken(fcmToken:String){
        let loggedUser = User.loggedUser

        let userRef = self.databaseRef.child(FIREBASE_USERS_TABLE)
        let userRefToFetch = userRef.child("\(loggedUser!.id!)")
        
        userRefToFetch.observeSingleEvent(of: DataEventType.value, with: {(snapshot: DataSnapshot?) in
            
            if (snapshot?.exists())! {
                if let snapshotDict = snapshot?.value as? Dictionary<String, Any?> {
                    let userhere = Mapper<User>().map(JSON: snapshotDict as [String : Any])
                    userhere?.fcm_token = fcmToken
                    userRefToFetch.setValue(userhere?.toJSON())
                }
            }
        })
    }
}
