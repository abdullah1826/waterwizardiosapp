//
//  Constants.swift
//  TravelPedia
//
//  Created by Muhammad Abdullah on 28/05/2019.
//  Copyright © 2019 Muhammad Abdullah - twaintec. All rights reserved.
//

import Foundation

var k_cartCounter = Int()
var didselectArray = String()


// we used static with variable because if we want to change anywhere during code we can
//easily assign a new value to this, it's a updateable value.
internal struct APIConstants {
    static var BasePath = "http://tacos.twaintec.com/api/"
}

internal struct APIPaths {
    static let takeorder = "takeorder"
    static let getorder = "getorder"
    static let deletecart = "deletecart"
    static let cancelOrder = "deleteorder"
    static let addcart = "addcart"
    static let updatecart = "updatecart"
    static let getcart = "getcart"
    static let getSignleCartItem = "getcartitem"
    static let submenu = "submenu"
    static let items = "items"
    static let signup = "signup"
    static let login = "login"
    static let cartCount = "cartcount"
    static let resetpassword = "resetpassword"
    static let translator = "translator"

}

internal struct NibName {
    static let searchViewController = "SearchViewController"
    static let cartListViewController = "CartListViewController"
    static let xibProductsCollectionViewCell = "XibCollectionViewCell"
    static let xibCategoriesCollectionViewCell = "CategoriesCollectionCell"

}
internal struct CellIdentifiers {
    
    static let k_sliderCell = "sliderCell"
    static let k_flashDealsCell = "flashDealsCell"
    static let k_newArrivalCell = "newArrivalCell"
    static let k_bestProductsCell = "bestProductsCell"
    static let k_categoriesCell = "categoriesCell"

}

internal struct Keys {
    static let Data = "data"
    static let Payload = "payload"
    static let accessToken = "accessToken"
    static let refreshToken = "refreshToken"
    static let userType = "notTenant"
    static let username = "userName"
    static let description = "description"
    static let errors = "errors"
    static let payload = "payload"
}

internal struct AlertConstants {
    static let Error = "Error!"
    static let DeviceType = "ios"
    static let Ok = "ok"
    static let EmailNotValid = "Email is not valid"
    static let PhoneNotValid = "Phone number is not valid"
    static let EmailEmpty = "Email is empty"
    static let PhoneEmpty = "Phone number is empty"
    static let FirstNameEmpty = "First name is empty"
    static let LastNameEmpty = "Last name is empty"
    static let NameEmpty = "Last name is empty"
    static let Empty = " is empty"
    static let PasswordsMisMatch = "Make sure your passwords match"
    static let LoginSuccess = "Login successful"
    static let SignUpSuccess = "Signup successful"
    static let PasswordInvalid = "Password is not valid"
    static let PasswordEmpty = "Password is empty"
    static let Success = "Success"
    static let InternetNotReachable = "Internet not reachable"
    static let UserNameEmpty = "Username is empty"
    static let TermsAndCondition = "Terms and conditions have not been accepted"
    static let AllFieldNotFilled = "Make sure all fields are filled"
    static let ProductAdded = "Added to cart"
}

internal struct Constants {
    static let Error = "Error!"
    static let DeviceType = "iOS"
    static let EmailRegix = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    static let PhoneRegix = "^((\\+)|(00))[0-9]{6,14}$"
    static let Done = "Done"
    static let Skip = "Skip"
    static let Signup = "Sign up"
    static let Signout = "Sign out"
    static let ResetPassword = "Reset Password"
    static let Dashboard = "Dashboard"
    static let Login = "Login"
    static let ForgotPassword = "Forgot Password"
    static let Settings = "Settings"
    static let Profile = "Profile"
    static let PrivacyPolicy = "Privacy Policy"
    static let PropertyList = "Property Listing"
    static let PropertyDetails = "Property Details"
    static let ProjectDetails = "Project Details"
    static let Filters = "Filters"
    static let TermsAndCondition = "Terms and Conditions"
    static let Bedrooms = "1"
    static let Bathrooms = "2"
    static let beds = "Beds"
    static let baths = "Baths"
    static let codeError = "Make sure you've entered the code properly"
    static let items = "items"
    static let counter = "counter"
    static let customer = "customer"
    static let user = "loggedUser"
    static let Empty = "empty"
    static let dummyValue = 0
    
}

internal struct DidselectRow {
    static let PH = "PH"
    static let CHLORINE = "CHLORINE"
    static let ALKALINITY = "ALKALINITY"
    static let BROMINE = "BROMINE"
    static let STABILIZER = "STABILIZER"
    static let ENZYMES = "ENZYMES"
    static let BREAKPOINT = "BREAKPOINT"
    static let ALGECIDE = "ALGECIDE"
    static let PHOSPHATE = "PHOSPHATE"
}

internal struct Chemical {
    static let brome = "brome.png"
    static let buffer = "buffer.png"
    static let chlor_out = "chlor_out.png"
    static let liquid_chlorine = "liquid_chlorine.png"
    static let muriatic_acid = "muriatic_acid.png"
    static let neutralizer = "neutralizer.png"
    static let ph_up = "ph_up.png"
    static let ph_down = "ph_down.png"
    static let shock_chlorine = "shock_chlorine.png"
    static let stabilizer = "stabilizer.png"
    static let unitGram = "g"
    static let unitMl = "ml"
    static let unitPpm = "ppm"

}


internal struct Theme {
    
//    static let primaryColor1 = UIColor(hexColor: "214097")
//    static let primaryColor2 = UIColor(hexColor: "3C70BA")
//    static let secondaryColor1 = UIColor(hexColor: "3d79BA")
//    static let secondaryColor2 = UIColor(hexColor: "3m79BA")
//    static let backColor = UIColor.white
//    static let blue = #colorLiteral(red: 0.231372549, green: 0.7529411765, blue: 0.9490196078, alpha: 1)
//    static let grayMain = #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 1)
//    static let blue = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
//    static let red = #colorLiteral(red: 0.9439367652, green: 0.1173132733, blue: 0, alpha: 1)
//    static let orangeSearch = UIColor(hexColor: "F07E22")
//    static let radioUnSelected = UIColor(hexColor: "E6E8ED")
}
