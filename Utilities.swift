import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class Utilites {
    static let shared = Utilites()
    static var RootNavController = UINavigationController()
    static var isLoggedIn = false
    static var selectedURL = ""
    static let appColor = #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1)
   // static let appFont = UIFont(name: "Nexa Light", size: 14)!
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    static func isInternetAvailable() -> Bool {
            guard Utilites.shared.reachabilityManager?.isReachable != nil else {return false}
        return true
    }
    
    private init() {
        
    }
    
    func printStuff() {
        print("stuff")
    }
    
    func getAPIError(json: JSON) -> String {
        return (json.dictionaryValue.compactMap { (key, value) -> String in "\(value)"} as Array)
            .joined(separator: "\n")
            .replacingOccurrences(of: "[", with: "")
            .replacingOccurrences(of: "]", with: "")
            .replacingOccurrences(of: "\"", with: "")
    }
    
    func getAttributedTexFromFile(path: URL, ext: NSAttributedString.DocumentType = .html) -> NSAttributedString? {
        do {
            let attributedStringWithRtf: NSAttributedString = try NSAttributedString(url: path, options: [NSAttributedString.DocumentReadingOptionKey.documentType: ext], documentAttributes: nil)
            return attributedStringWithRtf
        } catch let error {
            print("Got an error \(error)")
            return nil
        }
    }
    
//    func convertNumberToString(num: String) -> String {
//        switch num.count {
//        case 4, 5:
//            return "\((Float(num)!/1000).clean) " + self.getUnit(num: NSNumber(value: Float(1000))).components(separatedBy: " ").last!
//        case 6, 7:
//            return "\((Float(num)!/100000).clean) " + self.getUnit(num: NSNumber(value: Float(100000))).components(separatedBy: " ").last!
//        case 8, 9:
//            return "\((Float(num)!/10000000).clean) " + self.getUnit(num: NSNumber(value: Float(10000000))).components(separatedBy: " ").last!
//        default:
//            return num
//        }
//    }
    
    func getUnit(num: NSNumber) -> String {
        let numFormatter = NumberFormatter()
        numFormatter.locale = Locale(identifier: "en_IN")
        numFormatter.numberStyle = NumberFormatter.Style.spellOut
        return numFormatter.string(from: num) ?? ""
    }

    func commaSeperate(str: CGFloat) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "en_IN")
        numberFormatter.numberStyle = .decimal
        guard let formattedNumber = numberFormatter.string(from: NSNumber(value: Int(str))) else {return ""}
        return "PKR " + "\(formattedNumber)"
    }

    func isValidEmail(txt: String) -> Bool {
        let regEx = Constants.EmailRegix
        let pred = NSPredicate(format: "SELF MATCHES %@", regEx)
        return pred.evaluate(with: txt)
    }
    
    func email(to: String = "info@sirmaya.com") {
        guard let url = URL(string: "mailto://\(to)"), UIApplication.shared.canOpenURL(url) else {return}
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    func call(to: String = "080080080") {
        if let url = URL(string: "tel://\(to)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func secStyling(toView: UIView, _ radius: CGFloat = 20) {
        toView.layer.cornerRadius = radius
        toView.layer.shadowColor = UIColor.gray.cgColor
        toView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        toView.layer.shadowRadius = 2.0
        toView.layer.shadowOpacity = 0.7
    }
    
    func shouldShowReadMore(label: UILabel) -> Bool {
//        guard let text = label.text, let size = text.size(withAttributes: [NSAttributedString.Key.font: label.font]) as? CGSize else {return false}
//        return size.width > label.bounds.size.width ? true : false
        let size = label.text?.size(withAttributes: [NSAttributedString.Key.font: label.font!])
        if (size?.width)! > label.bounds.size.width {
            return true
        } else {
            return false
        }
    }
    
    func underlineLabel(label: UILabel, text: String, gesture: UITapGestureRecognizer) {
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(gesture)
        let wholeStr = label.text ?? ""
        let rangeToUnderLine = (wholeStr as NSString).range(of: text)
        let underLineTxt = NSMutableAttributedString(string: wholeStr,
                                                     attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),
                                                                  NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(1)])
        underLineTxt.addAttribute(NSAttributedString.Key.underlineStyle,
                                  value: NSUnderlineStyle.single.rawValue, range: rangeToUnderLine)
        label.attributedText = underLineTxt
    }
    
    
    func colorLabel(label: UILabel, text: String, gesture: UITapGestureRecognizer, _ color: UIColor = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)) {
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(gesture)
        let wholeStr = label.text ?? ""
        let rangeToUnderLine = (wholeStr as NSString).range(of: text)
        let underLineTxt = NSMutableAttributedString(string: wholeStr,
                                                     attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),
                                                                  NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(1)])
        underLineTxt.addAttributes([NSAttributedString.Key.foregroundColor: color], range: rangeToUnderLine)
        label.attributedText = underLineTxt
    }
    
}

extension String {
    var isPhoneNumber: Bool {
        let charcter  = NSCharacterSet(charactersIn: "03343442534").inverted
        var filtered: String!
        let inputString: NSArray = self.components(separatedBy: charcter) as NSArray
        filtered = inputString.componentsJoined(by: "") as String
        return  self == filtered
    }
}

