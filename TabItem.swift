//
//  TabItem.swift
//  CustomTabNavigation
//
//  Created by Sprinthub on 09/10/2019.
//  Copyright © 2019 Sprinthub Mobile. All rights reserved.
//

import UIKit

enum TabItem: String, CaseIterable {
    case Volume = "Volume"
    case Home = "Home"
    case Profile = "Profile"
    
    
    
    var viewController: UIViewController {
        switch self {
        case .Volume:
//            let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VolumeNavigation") as UIViewController
            // .instantiatViewControllerWithIdentifier() returns AnyObject! this must be downcast to utilize it
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let customViewController = storyboard.instantiateViewController(withIdentifier: "volumenavigation") as! UINavigationController
            
            
            
            return customViewController
        case .Home:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let customViewController1 = storyboard.instantiateViewController(withIdentifier: "homenavigation") as! UINavigationController
            
            return customViewController1
        case .Profile:
            var level = UserDefaults.standard.integer(forKey: "levelsaved")
            if level == 0{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                           let customViewController2 = storyboard.instantiateViewController(withIdentifier: "signinnavigation") as! UINavigationController
                return customViewController2
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let customViewController3 = storyboard.instantiateViewController(withIdentifier: "profilenavigation") as! UINavigationController
            return customViewController3
            }
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .Volume:
            return UIImage(named: "measuring-cup")!
        case .Home:
            return UIImage(named: "home (1)")!
        case .Profile:
            return UIImage(named: "user-1")!
        
        }
    }
    
    var displayTitle: String {
        return self.rawValue.capitalized(with: nil)
    }
}
