//
//  BaseViewController.swift
//
//  Created by Muhammad Abdullah on 27/03/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit


@available(iOS 13.0, *)

let k_backBtn = "back-white"

enum Storyboards {
    case Main
    var id: String {
        return String(describing: self)
    }
}

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true

    }
    
    func addBackBtn(){
                
        let backBtn = UIBarButtonItem(image: UIImage(named: k_backBtn)?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(tappedBackBtn))
            backBtn.tintColor = #colorLiteral(red: 0.01235525683, green: 0.3654455245, blue: 0.7603598237, alpha: 1)
            self.navigationItem.leftBarButtonItem = backBtn

    }
    
    @objc func tappedBackBtn(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func addRightBarBtn(){
        
        let backBtn = UIBarButtonItem(image: UIImage(named: "arrow-point-to-right.png"), style: .done, target: self, action: #selector(tappedRightBtn))
        backBtn.tintColor = .white
        self.navigationItem.rightBarButtonItem = backBtn

    }
    
    @objc func tappedRightBtn(){
        self.dismiss(animated: true, completion: nil)
        
    }

    
    func addDismissBtn(){
        
        let backBtn = UIBarButtonItem(image: UIImage(named: "arrow-point-to-right.png"), style: .done, target: self, action: #selector(tappedDismissBtn))
        backBtn.tintColor = .black
        self.navigationItem.leftBarButtonItem = backBtn

    }
    
    @objc func tappedDismissBtn(){
        self.dismiss(animated: true, completion: nil)
        
    }

    
    func pushController(controller toPush: String, storyboard: String) {
          let controller = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: toPush)
          self.navigationController?.pushViewController(controller, animated: true)
      }
      
      func getControllerRef(controller toPush: String, storyboard: String) -> UIViewController {
        return UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: toPush)

      }
    
    func present(controller toPresent: String, storyboard: String) {
        let controller = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(identifier: toPresent)
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func showAlertWith(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(AlertConstants.Ok.uppercased(), comment: ""), style: .`default`, handler: { _ in
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func showAlertWith(title: String, message: String, onSuccess closure: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(AlertConstants.Ok.uppercased(), comment: ""), style: .`default`, handler: { _ in
            closure()
        }))
        self.present(alert, animated: true, completion: nil)
    }

}
