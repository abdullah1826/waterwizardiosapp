//
//  AppGlobals.swift
//  AidedTradeApp
//
//  Created by Muhammad Abdullah on 19/12/2018.
//  Copyright © 2018 Muhammad Abdullah - twaintec. All rights reserved.
//

import UIKit

let RED = 69.0/255.0
let GREEN = 180.0/255.0
let BLUE = 77.0/255.0
let ALPHA = 1.0

let appDelegate = UIApplication.shared.delegate as! AppDelegate
@available(iOS 13.0, *)
let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as! SceneDelegate

class AppGlobals: NSObject {
    
    static let shared = AppGlobals()
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    
    var selectTabBaritem = Int()
    
    static let appColor = UIColor(red:  CGFloat(RED),
                                    green: CGFloat(GREEN),
                                    blue: CGFloat(BLUE),
                                    alpha: CGFloat(ALPHA))
    
    static let logo_greenColor = UIColor(red: 128.0/255.0, green: 195.0/255.0, blue: 36.0/255.0, alpha: 1.0)
    static let logo_pinkColor = UIColor(red: 218.0/255.0, green: 18.0/255.0, blue: 112.0/255.0, alpha: 1.0)
    static let logo_orangeColor = UIColor(red: 255.0/255.0, green: 147.0/255.0, blue: 0.0, alpha: 1.0)
    static let logo_blueColor = UIColor(red: 1.0/255.0, green: 80.0/255.0, blue: 146.0/255.0, alpha: 1.0)
    static let starRatingColor = UIColor(red: 218.0/255.0, green: 18.0/255.0, blue: 122.0/255.0, alpha: 1.0)
    static let GreenColor = UIColor(red: 69.0/255.0, green: 180.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    
//    static let customFont = UIFont(name: "Arvo-Bold", size: 14)!
//    static let customFontWith17 = UIFont(name: "Arvo-Bold", size: 17)!
//    
//    static let customNormalFont = UIFont(name: "Arvo", size: 12)!
//
//    static let customFontWith20 = UIFont(name: "Arvo-Bold", size: 20)!
//    static let customFontArvoBold12 = UIFont(name: "Arvo-Bold", size: 12)!
    
    
    static let FULL_SCREEN_WIDTH: CGFloat = CGFloat(UIScreen.main.bounds.width)
    static let FULL_SCREEN_HEIGHT: CGFloat = UIScreen .main.bounds.height
    static let tabBarSize : CGFloat = 50.0
    
    static let timerCheck : Bool = true

    private override init() {
        
        super.init()
        
    }
    
    class func appVersion() -> String {
        
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    class func appBuild() -> String {
        
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    
    //Accessible: can access easily by shared.methodName()
    func checkFontFamilyName(){
        
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    
    //Unaccessible: can't access class functions with shared word, it's can be accessible by class name.
    class func checkFontFamily() {
        
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
        
        
    }
    
    func customNavBarColor(){
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Arvo-Bold", size: 17) as Any,NSAttributedString.Key.foregroundColor : AppGlobals.logo_greenColor]
        
    }
    
    
    class func keypad(display:Bool, vu: UIView){
        
        if display == true {
            
            vu.endEditing(true)
            
        } else {
            
            vu.endEditing(false)
            
        }
        
    }
    
    class func isTextfieldEmpty(txtfld: UITextField) -> Bool {
        
        var value = Bool()
        
        if txtfld.text == "" {
            
            value = false
            
        } else {
            
            value = true
        }
        
        return value
    }
    
    class func isValid(name: String) -> Bool {
        
        // check the name is between 4 and 16 characters
        if !(4...16 ~= name.count) {
            
            return false
        }
        
        return true
    }
    
    func signInApi(username: String, password: String){
        
        
    }
    
    func changeTintColor(imageName: String, color: UIColor){
        
        let origImage = UIImage(named: imageName);
        _ = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        
    }
    
    
    func collectionViewWidth(collectionVu: UICollectionView) -> CGFloat {
        
        let collectionViewSize = collectionVu.bounds.size
        let width = collectionViewSize.width
        return width
        
    }
    
    func collectionViewHeight(collectionVu: UICollectionView) -> CGFloat {
        
        let collectionViewSize = collectionVu.bounds.size
        let height = collectionViewSize.height
        return height
        
    }
    
    class func moveDown(vu: UIView){
        
        UIView.animate(withDuration: 1.0, animations: {
            
            vu.frame.origin.y = 0
            vu.layoutIfNeeded()
            
        })
        
    }
    
    class func moveUp(vu: UIView, height: Int){
        
        UIView.animate(withDuration: 1.0, animations: {
            
            vu.frame.origin.y = CGFloat(-height)
            vu.layoutIfNeeded()
            
        })
        
    }
    
    func showActivityIndicatory(view: UIView, containerColor: UIColor?, loaderColor: UIColor?) {
        container.frame = view.frame
        container.center = view.center
        container.backgroundColor = containerColor != nil ? containerColor : UIColor.init(netHex: 0xffffff).withAlphaComponent(0.3) //UIColorFromHex(0xffffff, alpha: 0.3)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = view.center
        loadingView.backgroundColor =  loaderColor != nil ? loaderColor : UIColor.init(netHex: 0x444444).withAlphaComponent(0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.style =
            UIActivityIndicatorView.Style.white
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2,y :loadingView.frame.size.height / 2)
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        view.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator(view: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    func getTacos(dataDict: [String:Any]) -> [[String:Any]] {
        let tacosArray = dataDict["tacos"] as? [[String:Any]]
        return tacosArray!
    }
    
    func totalTacos(dataDict: [String:Any]) -> Int {
        let tacosArray = dataDict["tacos"] as? [[String:Any]]
        return tacosArray?.count ?? 0
    }
    
    func getDrinks(dataDict: [String:Any]) -> [[String:Any]] {
        let drinksArray = dataDict["drinks"] as? [[String:Any]]
        return drinksArray!
    }
    
    func totalDrinks(dataDict: [String:Any]) -> Int {
        let drinksArray = dataDict["drinks"] as? [[String:Any]]
        return drinksArray?.count ?? 0
    }

    
     //MARK:- saveCustomer
    

    // MARK:- redCustomer


    // MARK:- saveCartData
//    func saveData(_ cartItemsArray: [ResponseData]){
//        UserDefaults.standard.set(try? PropertyListEncoder().encode(cartItemsArray), forKey:Constants.items)
//        UserDefaults.standard.synchronize()
//    }
////
//    // MARK:- readCartData
//    class func readCartData() -> [ResponseData]{
//
//        var cartArray = [ResponseData]()
//        if let data = UserDefaults.standard.value(forKey:Constants.items) as? Data {
//            cartArray = try! PropertyListDecoder().decode(Array<ResponseData>.self, from: data)
//            print(cartArray)
//        }
//
//        return cartArray
//    }
//    func getCartCounter() -> Int {
//        return AppGlobals.readCartData().count
//    }

    // MARK:- readUserData
   
    
    class func removeUserData() {
        UserDefaults.standard.removeObject(forKey: Constants.user)
    }
    
    // MARK:- NavBar
    func hideNavBar(_ vc: UIViewController){
        vc.navigationController?.navigationBar.isHidden = true
    }
    
    func showNavBar(_ vc: UIViewController){
        vc.navigationController?.navigationBar.isHidden = false
    }
    
    
    
//    func getSelectedProductArray() -> [Json4Swift_Base] {
//        
//        switch  didselectArray {
//        case DidselectRow.flashDeals:
//            return (objectDict?.flashDeals)!
//        case DidselectRow.newArrival:
//            return (objectDict?.new_arrivals)!
//        default:
//            return (objectDict?.best_sellers)!
//        }
//    }
    

    
//    func saveCustomerData(_ customer: Customer) {
//        UserDefaults.standard.set(try? PropertyListEncoder().encode(customer), forKey:"customer")
//        
//
//    }
//    
//    class func readCustomerData() -> Customer{
//        
//        var customer = Customer
//        if let data = UserDefaults.standard.value(forKey:"customer") as? Data {
//            customer = try! PropertyListDecoder().decode(Dictionary<Customer>.self, from: data)
//            print(customer)
//            custome
//        }
//        
//        return customer
//    }
    
}
